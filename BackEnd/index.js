/**
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import express from "express"
import cors from "cors";
import userRouter from "./Routers/userRouter.js";
import orderListRouter from "./Routers/orderListRouter.js";
import orderRouter from "./Routers/orderRouter.js";
import {startUp} from "./Utils/entityLoader.js";
import authRouter from "./Routers/authRouter.js";
import {mailOrganisersOfExpiredLists} from "./Utils/orderlistExpiration.js";
import mailRouter from "./Routers/mailrouter.js";
import {generateRandomPassword} from "./Utils/helpers.js";


const app = express();
const port = 3000
app.use(cors());

app.use(express.json())
app.options('*', cors());

app.use(express.urlencoded({extended: true}))

/**
 * -------------------------
 * Using the Routers
 */
app.use("/users", userRouter)
app.use("/orders", orderRouter)
app.use("/orderlists", orderListRouter)
app.use("/auth", authRouter)
app.use("/mails", mailRouter)

/**
 * -------------------------
 */

app.listen(port, () => {
    startUp()
    mailOrganisersOfExpiredLists()
})