/**
 * Controller for the Order Entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import {orders} from "../Utils/entityLoader.js";
import {writeToFile} from "../Database/DatabaseHelper.js";
import {validateObject} from "../Utils/validations.js";
import {order_keys} from "../Database/dataTemplate.js";


let orderController = {};

/**
 * Function to list the Orders.
 * When no query params are given, it will return all orders
 * When query params are given, it will return all orders matching the params
 *
 * @param req
 * @param res
 */
orderController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the order, it'll be added to 'result'
    const result = orders.filter(order => {
        let isValid = true;

        for(const key in queryParams) {
            isValid = isValid && order[key] == queryParams[key]
        }
        return isValid;
    });

    res.json(result);
}

/**
 * Function to get a specific order.
 * It will search for an order matching the url parameter - ID.
 * When the order with the given ID does not exist, it'll return an error
 * @param req
 * @param res
 */
orderController.get = (req, res) => {
    const order = orders.find((product) => {
        return req.params.id === product.id;
    });

    if(order) {
        res.status(200).json(order);
    } else {
        res.status(404).send("Order not found!");
    }
}

/**
 * Function to post a new order
 * You can not provide a new ID, as it will be automatically generated.
 *
 * @param req
 * @param res
 * @returns {*}
 */
orderController.post = (req, res) => {
    let order = req.body;

    if (order.id) {
        return res.status(400).send("Do not provide ID")
    }
    if (!validateObject(order, order_keys)) {
        return res.status(400).send("Wrong data provided")
    }
    order.id = uuid();
    order.userID = req.user.id;
    order.isPayed = false;
    orders.push(order);
    res.status(201).json(order)
    writeToFile("orders", orders).catch((err) => {
        console.log(err)
    });
}

/**
 * Function to PUT the order with the given ID.
 * The ID should not change, so you can not provide a new one.
 *
 * @param req
 * @param res
 * @returns {*}
 */
orderController.put = (req, res) => {
    let newOrder = req.body;
    let orderIndex = orders.findIndex((product) => {
        return req.params.id === product.id;
    });

    if (newOrder.id) {
        return res.status(400).send("Do not provide ID")
    }
    if (!validateObject(newOrder, order_keys)) {
        return res.status(400).send("Wrong data provided")
    }

    newOrder.id = orders[orderIndex].id;
    newOrder.userID = orders[orderIndex].userID;
    newOrder.orderlistID = orders[orderIndex].orderlistID;
    orders[orderIndex] = newOrder;
    res.status(200).json(newOrder);
    writeToFile("orders", orders).catch((err) => {
        console.log(err)
    });
}

/**
 * Function to delete the order with the given ID.
 * If the order does not exist, an error will be send.
 * @param req
 * @param res
 */
orderController.delete = (req, res) => {
    let orderIndex = orders.findIndex((product) => {
        return req.params.id === product.id;
    });

    if(orderIndex === -1) {
        res.status(404).send("Order not found!");
    } else {
        orders.splice(orderIndex, 1);
        res.sendStatus(200);
        writeToFile("orders", orders).catch((err) => {
            console.log(err)
        });
    }
}

export default orderController;