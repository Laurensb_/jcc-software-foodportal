/**
 * Controller for handling mails
 * Created by Laurens Bittner and Thijs van der Vegt
 */
import {findOrderListByUuid} from "../Utils/helpers.js";
import {sendMailToOrganiserOfOrderList, sendMailToUserRequestingPass} from "../Utils/mailService.js";

let mailController = {}

/**
 * Function to post to "/mails" -> Create an email based on type
 * @param req
 * @param res
 */

let types = ["maillist", "emailconfirm", "", ""] //put types of emails in here. the request will be checked for a type, so we know what kind of email to send

mailController.post = (req, res) => {
    if (!req.body.type) {
        return res.status(400).send("no type provided")
    }

    if (req.body.type.toLowerCase() === "maillist") {
        console.log("test")
        sendOrderListMail(req.body.orderlistID)
        return res.status(200)
    }

    if (req.body.type.toLowerCase() === "mailpass") {
        console.log("test")
        sendMailToUserRequestingPass(req.body.email)
        return res.status(200)
    }

    return res.status(404).send("type not found")
}

/**
 * Function to send the content of an orderlist to the organiser
 * @param id -> of list to send
 */
function sendOrderListMail(id) {
    let list = findOrderListByUuid(id)
    sendMailToOrganiserOfOrderList(list, "Your requested orderList")
}

export default mailController