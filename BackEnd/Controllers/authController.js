/**
 * Controller for handling authentication
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {users} from "../Utils/entityLoader.js";
import bcrypt from 'bcrypt';
import jwt from "jsonwebtoken";

let authController = {}

/**
 * Function to post to "/auth" -> Logging in
 * @param req
 * @param res
 */
authController.post = (req, res) => {
    const { email, password } = req.body;

    if(email && password) {
        const token = login(email, password);
        if(token) {
            res.status(201).send({token: token});
        } else {
            res.status(401).send("Email and/or password incorrect");
        }
    } else {
        res.status(400).send("Required parameters missing");
    }
}

/**
 * Function to Log in and get the token.
 *
 * @param email with which you log in
 * @param password with which you log in
 * @returns {*|boolean} The token || false
 */
const login = (email, password) => {
    const user = users.find((user) => {
        return user.email === email;
    });

    // comparing the given password with the password of the user
    if(user && user.password) {
        const result = bcrypt.compareSync(password, user.password);

        // returning the token
        if(result) {
            return jwt.sign({
                id: user.id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                roles: user.roles
            }, user.secret);
        }
    }

    return false;
};

export default authController