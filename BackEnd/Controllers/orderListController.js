/**
 * Controller for the OrderList Entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import {orderLists} from "../Utils/entityLoader.js";
import {writeToFile} from "../Database/DatabaseHelper.js";
import {validateObject} from "../Utils/validations.js";
import {orderlist_keys} from "../Database/dataTemplate.js";
import {sendMailToUsersByIDList} from "../Utils/mailService.js";
import orderController from "./orderController.js";
import {createHTMLForInvite} from "../Utils/helpers.js";

let orderListController = {};

/**
 * Function to list the OrderLists
 * When no query params are given, it will return all auctions.
 * When query params are given, it will return all auctions matching the params.
 *
 * @param req
 * @param res
 * @returns {*}
 */
orderListController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the orderList, it'll be added to 'result'
    const result = orderLists.filter(orderlist => {
        let isValid = true;

        for(const key in queryParams) {
            isValid = isValid && orderlist[key] == queryParams[key]
        }
        return isValid;
    });

    // If the User is an admin, it will see all orderLists, if not, the user will only see the orderLists where he is invited to.
    if(req.user.roles.includes("admin")) {
        return res.json(result);
    }

    const userResult = result.filter(orderlist => {
        return orderlist.invitedUsers.includes(req.user.id);
    });

    return res.json(userResult)
}

/**
 * Function to get a specific orderList.
 * It will search for an orderList matching the url parameter - ID.
 * When the orderList with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res
 */
orderListController.get = (req, res) => {
    const orderList = orderLists.find((product) => {
        return req.params.id === product.id;
    });

    if(orderList) {
        res.status(200).json(orderList);
    } else {
        res.status(404).send("OrderList not found!");
    }
}

/**
 * Function to get all the orders from an orderList with the given ID.
 * Uses the list function from the orderController
 *
 * @param req
 * @param res
 */
orderListController.getOrders = (req, res) => {
    req.query = {
        orderlistID: req.params.id
    }
    return orderController.list(req, res);
}

/**
 * Function to post a new OrderList
 * You can not provide a new ID, as it will be automatically generated
 * The organiser of the orderList will automatically be set to the logged in User
 *
 * @param req
 * @param res
 * @returns {*}
 */
orderListController.post = (req, res) => {
    let orderList = req.body;

    if (orderList.id) {
        return res.status(400).send("Do not provide ID")
    }
    if (!validateObject(orderList, orderlist_keys)) {
        return res.status(400).send("Wrong data provided")
    }

    // The organiser will automatically be invited to the list
    if(!orderList.invitedUsers.includes(req.user.id)) {
        orderList.invitedUsers.push(req.user.id)
    }

    orderList.organiser = req.user.id
    orderList.id = uuid()
    orderList.hasEnded = false
    orderLists.push(orderList)
    res.status(201).json(orderList)
    writeToFile("orderlists", orderLists).catch((err) => {
        console.log(err)
    })

    // Content of the email that will be send to every invited user
    let content = {
        subject: "Uitnodiging om te bestellen",
        text: "Click the link to place an order",
        html: createHTMLForInvite(orderList)
    }

    sendMailToUsersByIDList(orderList.invitedUsers, content)
}

/**
 * Function to PUT the orderList with the given ID.
 * The ID should not change, so you can not provide a new one.
 * @param req
 * @param res
 * @returns {*}
 */
orderListController.put = (req, res) => {
    let newOrderList = req.body;

    // Find the index of the orderList with the given ID in the complete orderLists list.
    let orderListIndex = orderLists.findIndex((product) => {
        return req.params.id === product.id;
    });

    if (newOrderList.id) {
        return res.status(400).send("Do not provide ID")
    }
    if (!validateObject(newOrderList, orderlist_keys)) {
        return res.status(400).send("Wrong data provided")
    }

    newOrderList.id = orderLists[orderListIndex].id;
    newOrderList.hasEnded = orderLists[orderListIndex].hasEnded;
    newOrderList.organiser = orderLists[orderListIndex].organiser;

    // If the organiser did not invite himself manually, he will be invited automatically.
    if(!newOrderList.invitedUsers.includes(req.user.id)) {
        newOrderList.invitedUsers.push(req.user.id)
    }

    orderLists[orderListIndex] = newOrderList;
    res.status(200).json(newOrderList);
    writeToFile("orderlists", orderLists).catch((err) => {
        console.log(err)
    })
}

/**
 * Function to delete the orderList with the given ID
 * If the orderList does not exist, an error will be send.
 * @param req
 * @param res
 */
orderListController.delete = (req, res) => {
    let orderListIndex = orderLists.findIndex((product) => {
        return req.params.id === product.id;
    })

    if(orderListIndex === -1) {
        res.status(404).send("OrderList not found!");
    } else {
        orderLists.splice(orderListIndex, 1);
        res.sendStatus(200)
        writeToFile("orderlists", orderLists).catch((err) => {
            console.log(err)
        })
    }
}

export default orderListController