/**
 * Controller for the User Entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import {users} from "../Utils/entityLoader.js";
import bcrypt from 'bcrypt';
import {writeToFile} from "../Database/DatabaseHelper.js";
import {validatePassword, validateEmail, validateObject} from "../Utils/validations.js";
import {user_keys} from "../Database/dataTemplate.js";

let userController = {};

/**
 * Function to list the Users
 * When no query params are given, it will return all Users.
 * When query params given, it will return all Users matching the params.
 *
 * @param req
 * @param res list with users
 */
userController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the user, it'll be added to 'result'
    const result = users.filter(user => {
        let isValid = true;
        for(const key in queryParams) {
            isValid = isValid && user[key] == queryParams[key]
        }
        return isValid;
    });

    // An Admin will get the whole User object. A User will only get certain properties.
    if(req.user.roles.includes("admin")) {
        res.json(result);
    } else {
        const userResult = result.map(selectFewerProps)
        res.json(userResult);
    }
}

/**
 * Function to get a specific User.
 * It will search for a User matching the url parameter - ID.
 * When the User with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res the specific User || Error 404
 */
userController.get = (req, res) => {
    const user = users.find((product) => {
        return req.params.id === product.id
    });

    if(user) {
        res.status(200).json(user);
    } else {
        res.sendStatus(404);
    }
}

/**
 * Function to post a new User
 * You can not provide a new ID as well as the secret, as it will be automatically generated.
 * A new User will automatically be a 'user'.
 * It will also validate the email and password.
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
userController.post =  async (req, res) => {
    let user = req.body;

    if(user.id) {
        return res.status(400).send("You should not provide an ID");
    }
    if (!validateObject(user, user_keys)) {
        return res.status(400).send("Wrong data provided")
    }
    if(!validateEmail(user.email)) {
        return res.status(400).send("Email is not valid")
    }
    if(!validatePassword(user.password)) {
        return res.status(400).send('password is not valid')
    }

    // Hashing the password using Bcrypt
    user.password = await hashPassword(res, user.password).catch((err) => {
        console.log(err)
    });

    user.id = uuid();
    user.secret = uuid();
    user.roles = ["user"]
    users.push(user);
    res.status(201).json(user);
    writeToFile("users", users).catch((err) => {
        console.log(err)
    });
}

/**
 * Function to PUT the User with the given ID.
 * The ID should not change, so you can not provide a new one.
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
userController.put = async (req, res) => {
    let newUser = req.body;
    console.log(newUser)

    let userIndex = users.findIndex((product) => {
        return req.user.id === product.id
    });

    let user = users[userIndex];

    if (newUser.oldPass && newUser.newPass) {
        const result = bcrypt.compareSync(newUser.oldPass, user.password)

        if (!result) {
            return res.status(401).send({error: "Old password was incorrect"})
        }

        user.password = await hashPassword(res, newUser.newPass).catch((err) => {
            console.log(err)
        });

    } else if (newUser.firstName && newUser.lastName && newUser.email) {
        user.firstName = newUser.firstName
        user.lastName = newUser.lastName
        user.email = newUser.email
    } else {
        return res.status(404).send({error: "values to change not found"})
    }

    console.log(user)

    users[userIndex] = user;
    res.status(200).json(newUser)
    writeToFile("users", users).then().catch((err) => {
        console.log(err)
    });
}

/**
 * Function to delete the User with the given ID.
 * If the User does not exist, an error will be send.
 *
 * @param req
 * @param res
 */
userController.delete = (req, res) => {
    let userIndex = users.findIndex((product) => {
        return req.params.id === product.id
    });

    if(userIndex === -1) {
        res.sendStatus(404);
    } else {
        users.splice(userIndex, 1);
        res.sendStatus(200);
        writeToFile("users", users).catch((err) => {
            console.log(err)
        });
    }
}

/**
 * Function to hash a password, using Bcrypt
 * @param res
 * @param password The password that will be hashed
 * @returns {Promise<*>}
 */
async function hashPassword(res, password) {
    try {
        return await bcrypt.hash(password, 10);
    } catch {
        res.sendStatus(500);
    }
}

/**
 * Helper function used in the 'list' function
 * Used for selecting certain properties from an array with users
 * @param show
 * @returns {{firstName, lastName, id, email}}
 */
function selectFewerProps(show) {
    const {id, firstName, lastName, email} = show;
    return {id, firstName, lastName, email};
}

export const changePassword = (pass, email) => {
    if (email) {
        let userIndex = users.findIndex((product) => {
            return email === product.email
        });

        console.log(pass)
        hashPassword(10, pass).then(password => {
           users[userIndex].password = password
            console.log(password)

            writeToFile("users", users).catch((err) => {
                console.log(err)
            });
        })
    }
}

export default userController;