/**
 * Middleware to check if the user is an Admin or not.
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {users} from "../Utils/entityLoader.js";

const isAdmin = (req, res, next) => {
    const email = req.user.email;

    // Finding the User that matches the username of the user that is logged in (Based on the token)
    const user = users.find((user) => {
        return email === user.email;
    });

    // If the user is an admin, return next, and if the user is not an admin, send an error.
    if(user.roles.includes('admin')) {
        return next();
    }

    res.status(403).send("this action needs admin privileges!")
}

export default isAdmin;