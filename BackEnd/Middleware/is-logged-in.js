/**
 * Middleware to check if the user is logged in or not.
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import jwt from "jsonwebtoken";
import {users} from "../Utils/entityLoader.js"

const isLoggedIn = (req, res, next) => {
    const token = getTokenFromRequest(req);

    if(token) {
        const payload = verifyToken(token);
        if(payload) {
            req.user = payload;
            return next();
        }
    }
    res.status(401).send("authentication failed")
}

/**
 * Function to get the token from the request.
 * @param req
 * @returns {boolean|*}
 */
const getTokenFromRequest = (req) => {
    const authHeader = req.headers['authorization'];

    if(authHeader) {
        return authHeader.split(' ')[1];
    }

    return false;
}

/**
 * Function to verify the token, to make sure that the token is actually correct.
 * @param token
 * @returns {boolean|*}
 */
const verifyToken = (token) => {
    const tokenPayLoad = jwt.decode(token);

    if(tokenPayLoad) {
        const user = users.find(user => user.email === tokenPayLoad.email);

        if(user) {
            try {
                return jwt.verify(token, user.secret);
            } catch (e) {
                return false;
            }
        }
    }
    return false;
}

export default isLoggedIn;