/**
 * Router for the User Entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import Express from "express";
import userController from "../Controllers/userController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";

const userRouter = Express.Router();

/**
 * Where the route = "/users":
 * You can GET and POST Users
 * The GET request requires the user to be logged in.
 * POSTING a new user is Registering, so you don't have to be logged in for this of course.
 */
userRouter
    .route("/")
    .get(isLoggedIn, userController.list)
    .post(userController.post);

/**
 * Where the route ="/users/:id":
 * You can GET PUT and DELETE Users
 */
userRouter
    .route("/:id")
    .get(isLoggedIn, userController.get)
    .put(isLoggedIn, userController.put)
    .delete(isLoggedIn, userController.delete)

export default userRouter;