/**
 * Router for handling authentication -> logging in
 * Created by Laurens Bittner and Thijs van der Vegt
 */
import Express from "express";
import authController from "../Controllers/authController.js";

const authRouter = Express.Router();

/**
 * Where the route = "/auth"
 * You can POST a.k.a log in.
 */
authRouter.post("/", authController.post);

export default authRouter