/**
 * Router for the OrderList entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import Express from "express";
import orderListController from "../Controllers/orderListController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";

const orderListRouter = Express.Router();

/**
 * Where the route = "/orderlists"
 * You can GET and POST orderLists
 * You have to be logged in for every request
 */
orderListRouter
    .route("/")
    .get(isLoggedIn, orderListController.list)
    .post(isLoggedIn, orderListController.post);

/**
 * Where the route = "/orderlists/:id"
 * You can GET, PUT, and DELETE orderLists
 * every request requires you to be logged in.
 */
orderListRouter
    .route("/:id")
    .get(isLoggedIn, orderListController.get)
    .put(isLoggedIn, orderListController.put)
    .delete(isLoggedIn, orderListController.delete)

/**
 * Where the route = "/orderlists/:id/orders"
 * You can GET orders of the orderList with the given ID
 * You have to be logged in for the request
 */
orderListRouter
    .route("/:id/orders")
    .get(isLoggedIn, orderListController.getOrders)

export default orderListRouter;