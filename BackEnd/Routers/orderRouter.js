/**
 * Router for the Order Entity
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import Express from "express";
import orderController from "../Controllers/orderController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";

const orderRouter = Express.Router();

/**
 * Where the route = "/orders"
 * You can GET and POST orders
 * You have to be logged in for every request
 */
orderRouter
    .route("/")
    .get(isLoggedIn, orderController.list)
    .post(isLoggedIn, orderController.post);

/**
 * Where the route = "/orders/:id"
 * You can GET, PUT, and DELETE orders
 * You have to be logged in for every request
 */
orderRouter
    .route("/:id")
    .get(isLoggedIn, orderController.get)
    .put(isLoggedIn, orderController.put)
    .delete(isLoggedIn, orderController.delete)

export default orderRouter;