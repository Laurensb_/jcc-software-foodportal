### Get all Orderlists
GET http://localhost:3000/orderlists
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Test if the Response is an Array (Getting everything, should return an array)", function() {
   client.assert(Array.isArray(response.body), "Response is not an Array");
});
%}

### Get all the orderlists where restaurantName = "McDonalds" (Filtering on one item)
GET http://localhost:3000/orderlists?restaurantName=McDonalds
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Test if the Response is an Array (Getting everything, should return an array)", function() {
   client.assert(Array.isArray(response.body), "Response is not an Array");
});
%}

### Get all the orderlists with a restaurantName that does not exist -> "RestaurantThatDoesNotExist" (Filtering on an item value that does not exist)
GET http://localhost:3000/orderlists?restaurantName=RestaurantThatDoesNotExist
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Test if the Response is an Array (Getting everything, should return an array)", function() {
   client.assert(Array.isArray(response.body), "Response is not an Array");
});
client.test("Test if the Array is Empty (It returns an empty Array", function() {
    client.assert((response.body).length === 0, "Array is not empty")
})
%}

### Get Orderlist with id: "ed31f609-6f3d-4dce-a397-da643baa603f"
GET http://localhost:3000/orderlists/ed31f609-6f3d-4dce-a397-da643baa603f
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Request executed successfully", function() {
    client.assert(response.body.id === "ed31f609-6f3d-4dce-a397-da643baa603f", "Requested ID and Given ID do not match");
 });
 %}

### Get an orderlist that does not exist
GET http://localhost:3000/orderlists/id_that_doesnt_exist
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 404", function() {
   client.assert(response.status === 404, "Response status is not 404!");
});
 %}

### Get all orders on the orderlist with id "afd7d144-dfbe-422b-ab59-8ede038404a7"
GET http://localhost:3000/orderlists/afd7d144-dfbe-422b-ab59-8ede038404a7/orders
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Test if the Response is an Array (Getting everything, should return an array)", function() {
   client.assert(Array.isArray(response.body), "Response is not an Array");
});
%}

### Post a new orderlist (Without providing an ID)
POST http://localhost:3000/orderlists
Content-Type: application/json
Authorization: Bearer {{token}}

{
  "title": "NewList",
  "restaurantName": "McDonalds",
  "linkToMenu": "https://google.nl",
  "endDate": "2021-12-30T18:41:22.607Z",
  "invitedUsers": [
  ]
}

> {%
client.test("Test if the Response Status equals 201", function() {
   client.assert(response.status === 201, "Response status is not 201");
});
 %}

### Post a new Orderlist (Providing an ID)
POST http://localhost:3000/orderlists
Content-Type: application/json
Authorization: Bearer {{token}}

{
  "id": "test",
  "title": "NewList",
  "restaurantName": "McDonalds",
  "linkToMenu": "google.nl het zelf ff",
  "endDate": "2021-12-30T18:41:22.607Z",
  "invitedUsers": []
}

> {%
client.test("Test if the Response Status equals 400", function() {
    client.assert(response.status === 400, "Response status is not 400");
});
 %}

### Update orderlist with ID: afd7d144-dfbe-422b-ab59-8ede038404a7 (Without providing an ID)
#PUT http://localhost:3000/orderlists/afd7d144-dfbe-422b-ab59-8ede038404a7
#Content-Type: application/json
#Authorization: Bearer {{token}}
#
#{
#  "title": "updated",
#  "restaurantName": "McDonalds",
#  "linkToMenu": "google.nl het zelf ff",
#  "endDate": "2021-12-30T18:41:22.607Z",
#  "invitedUsers": []
#}
#
#> {%
#client.test("Test if the Response Status equals 200", function() {
#    client.assert(response.status === 200, "Response Status is not 200!");
#});
#client.test("Test if the title gets updated", function() {
#    client.assert(response.body.title === "updated", "Title has not been updated (If the title previously was not 'updated'");
#});
# %}

### Update orderlist with ID: afd7d144-dfbe-422b-ab59-8ede038404a7 (Providing an ID)
PUT http://localhost:3000/orderlists/afd7d144-dfbe-422b-ab59-8ede038404a7
Content-Type: application/json
Authorization: Bearer {{token}}

{
  "id": "test",
  "title": "updated",
  "restaurantName": "McDonalds",
  "linkToMenu": "google.nl het zelf ff",
  "endDate": "2021-12-30T18:41:22.607Z",
  "invitedUsers": []
}

> {%
client.test("Test if the Response Status equals 400", function() {
    client.assert(response.status === 400, "Response Status is not 400!");
});
%}

### Delete orderlist with ID afd7d144-dfbe-422b-ab59-8ede038404a7
#DELETE http://localhost:3000/orderlists/afd7d144-dfbe-422b-ab59-8ede038404a7
#Authorization: Bearer {{token}}
#
#> {%
#client.test("Test if the Response Status equals 200", function() {
#    client.assert(response.status === 200, "Response Status is not 200!");
#});
# %}

### Delete an orderlist that does not exist
DELETE http://localhost:3000/orderlists/id_that_doesnt_exist
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 404", function() {
    client.assert(response.status === 404, "Response Status is not 404!");
})
 %}
