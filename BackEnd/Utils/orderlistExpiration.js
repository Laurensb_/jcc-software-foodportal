/**
 * File which handles the expiration of orderLists
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {orderLists} from "./entityLoader.js";
import {sendMail, sendMailToOrganiserOfOrderList} from "./mailService.js";
import {writeToFile} from "../Database/DatabaseHelper.js";
import {convertListOfOrdersToHTMLString, findUserByUuid, getListOfOrdersFromOrderListID} from "./helpers.js";

/**
 * Function to send a mail to the organisers of orderList which are expired.
 * Will check every orderList every minute if it has expired.
 *
 * Expired? -> Send mail to the Organiser
 * The organiser can then use that mail, to order the food.
 */
export function mailOrganisersOfExpiredLists() {
    console.log("expiredCheck " + new Date)
    let changed = false
    orderLists.forEach(orderList => {
        if (!orderList.hasEnded && new Date(orderList.endDate).getTime() < Date.now()) {
            sendMailToOrganiserOfOrderList(orderList, "OrderList expired")
            orderList.hasEnded = true
            changed = true
        }
    })
    if (changed) {
        writeToFile("orderlists", orderLists).catch((err) => {
            console.log(err)
        });
    }
    // Check every minute
    setTimeout(() => {
        mailOrganisersOfExpiredLists()
    }, 60000);
}