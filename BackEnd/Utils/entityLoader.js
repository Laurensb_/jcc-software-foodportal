/**
 * File to load the entities.
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {readFromFile} from "../Database/DatabaseHelper.js";

let users;
let orders;
let orderLists;

/**
 * Function to read all .json files. Used when starting the application.
 * Makes sure the data is up to date.
 */
function startUp () {
    users = readFromFile("users")
    orders = readFromFile("orders")
    orderLists = readFromFile("orderlists")
}

export {
    users, orders, orderLists, startUp
}