/**
 * Validator file
 * Created by Laurens Bittner and Thijs van der Vegt
 */

/**
 * Function to validate an object, based on given keys
 *
 * @param object the object that needs validation
 * @param keys the keys uses for validating
 * @returns {boolean} valid || invalid
 *
 * The object should contain and only contain the keys from 'keys'. If the keys are not the same -> return false
 */
export function validateObject(object, keys) {
    let valid = true;

    Object.keys(object).forEach(key => {
        if (!keys.includes(key)) {
            valid = false
        }
    })

    return valid
}

/**
 * Function to validate an Email
 *
 * @param email to be validated
 * @returns {boolean} valid || invalid
 *
 * Checks if the Email contains an '@',
 * Checks if there is text before the '@', and text after the '@'
 * Checks if the Email ends with either '.com' or '.nl'
 *
 * Automatically converts the Email to lowercase
 */
export function validateEmail(email) {
    const re = /(\w+.)+@(\w*.)+\.(com|nl)/g;
    return re.test(String(email).toLowerCase());
}

/**
 * Function to validate a Password
 *
 * @param password to be validated
 * @returns {boolean} valid || invalid
 *
 * Checks if the Password contains at least 1 lowercase letter
 * Checks if the Password contains at least 1 capital letter
 * Checks if the Password contains at least 1 number
 * Checks if the Password is at least 6 characters long
 */
export function validatePassword(password) {
    const re =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/g;
    return re.test(String(password));
}