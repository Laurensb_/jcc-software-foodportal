/**
 * File with helper functions
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import {orderLists, orders, users} from "./entityLoader.js";

/**
 * Function to get all the orders from the orderList with the given ID.
 *
 * @param listID -> ID of the list you want to get the orders from
 * @returns {*[]}
 */
export function getListOfOrdersFromOrderListID(listID) {
    let orderList = [];

    orders.forEach(order => {
        if (order.orderlistID === listID) {
            orderList.push(order)
        }
    })

    return orderList
}

/**
 * Function to the user with the given UUID (ID)
 * @param uuid -> ID of the user you want to find
 * @returns {*}
 */
export function findUserByUuid(uuid) {
    return users.filter(user => {
        return user.id === uuid
    })[0]
}

/**
 * Function to the orderlist with the given UUID (ID)
 * @param uuid -> ID of the list you want to find
 * @returns {*}
 */
export function findOrderListByUuid(uuid) {
    return orderLists.filter(list => {
        return list.id === uuid
    })[0]
}

/**
 * Function to convert the List of Orders to HTML
 * Used for sending mails.
 * @param listOfOrders
 * @returns {string}
 */
export function convertListOfOrdersToHTMLString(listOfOrders) {
    let HTMLString = "<table style='border-collapse: separate; border-spacing: 0;'>"
    let thStart = "<th style='text-align: center; border: 1px solid black; color: white; background: #7F1D3C; min-width: 10em; max-width: 30em;'>"
    let tdStart = "<td style='text-align: center; border: 1px solid black; background: #909294; min-width: 10em; max-width: 30em;'>"

    HTMLString += `<tr>${thStart}Persoon</th>${thStart}Betaalmethode</th>${thStart}Beschrijving</th></tr>`

    listOfOrders.forEach(order => {
        let user = findUserByUuid(order.userID)
        HTMLString += `<tr>${tdStart}${user.firstName + " " + user.lastName}</td>${tdStart}${order.paymentMethod}</td>${tdStart}${order.description}</td></tr>`;
    })
    HTMLString += "<table>"
    return HTMLString
}

/**
 * Function to create an invite from a template with information from an orderlist
 * Used for sending mails.
 * @param orderlist
 * @returns {string}
 */

export function createHTMLForInvite(orderlist) {

    let organiser = findUserByUuid(orderlist.organiser)

    return` 
<div style="display: inline-block; padding: 2em; border: solid #7F1D3C; border-radius: 1em; background: #969696;">
    <table style="padding: 2em; width: 100%; text-align: center; border: 1px solid black; border-radius: 1em; background: white;">
        <tbody>
            <tr><td><h2>Uitnodiging voor lijst: ${orderlist.title}</h2></td></tr>
            <tr><td><h4>Restaurant: ${orderlist.restaurantName}</h4></td></tr>
            <tr><td><h4>Organisator: ${organiser.firstName} ${organiser.lastName}</h4></td></tr>
            <tr><td><a href="http://localhost:5000/orderlist/${orderlist.id}" style="margin-left: auto; padding: 0.2em; border-radius: .35em; color: rgba(255, 255, 255, 1); text-decoration: none; background-color: rgba(127, 29, 60, 0.7);">Klik hier om een bestelling te plaatsen</a></td></tr>
        </tbody>
    </table>
</div>`
}

export function createHTMLForTempPass(password) {
    return `<div style="display: inline-block; padding: 2em; border: solid #7F1D3C; border-radius: 1em; background: #969696;">
    <table style="padding: 2em; width: 100%; text-align: center; border: 1px solid black; border-radius: 1em; background: white;">
        <tbody>
            <tr><td><h2>Tijdelijk wachtwoord aangemaakt</h2></td></tr>
            <tr><td><h4>Wachtwoord: ${password}</h4></td></tr>
            <tr><td><h3>Verander zo snel mogelijk uw wachtwoord!</h3></td></tr>
            <tr><td><a href="http://localhost:5000/login" style="margin-left: auto; padding: 0.2em; border-radius: .35em; color: rgba(255, 255, 255, 1); text-decoration: none; background-color: rgba(127, 29, 60, 0.7);">Klik hier om in te loggen</a></td></tr>
        </tbody>
    </table>
</div>`
}

export function generateRandomPassword(length) {
    const capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    const charset = "abcdefghijklmnopqrstuvwxyz0123456789" + capitals;
    let password = "";

    for (let i = 0, n = charset.length; i < length; ++i) {
        password += charset.charAt(Math.floor(Math.random() * n))
    }

    password += capitals.charAt(Math.floor(Math.random() * capitals.length))

    return password
}