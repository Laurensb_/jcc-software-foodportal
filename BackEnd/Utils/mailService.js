/**
 * MailService - Sending Mails
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import nodemailer from "nodemailer";
import {users} from "./entityLoader.js";
import {
    convertListOfOrdersToHTMLString, createHTMLForTempPass,
    findUserByUuid,
    generateRandomPassword,
    getListOfOrdersFromOrderListID
} from "./helpers.js";
import {changePassword} from "../Controllers/userController.js";

/**
 * Account which will send the mails
 * @type {Mail}
 */
let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user: "jccmailservice@gmail.com",
        pass: "JCCFoodStorage123@",
    },
});

/**
 * Function to send a Mail
 *
 * @param recipient
 * @param subject
 * @param text
 * @param html
 * @returns {Promise<void>}
 */
export async function sendMail(recipient, subject, text, html) {

    let info = transporter.sendMail({
        from: "jccmailservice@gmail.com",
        to: recipient,
        subject: subject,
        text: text,
        html: html,
    });
}

/**
 * Function to send a mail to every User in a given list with User ID's
 *
 * @param idList -> List with User ID's you want to send a mail to
 * @param content
 */
export function sendMailToUsersByIDList(idList, content) {
    idList.forEach(id => {
        users.forEach(user => {
            if (user.id === id) {
                sendMail(user.email, content.subject, content.text, content.html).catch((err) => {
                    console.log(err)
                })
            }
        })
    })
}

/**
 * Function to send a mail to every User in a given list with User ID's
 *
 * @param orderList -> orderlist you want to be mailed
 * @param subject -> subject of the mail
 */
export function sendMailToOrganiserOfOrderList(orderList, subject) {
    let organiser = findUserByUuid(orderList.organiser)

    let orders = getListOfOrdersFromOrderListID(orderList.id)
    let HTML = convertListOfOrdersToHTMLString(orders)


    if (organiser) {
        sendMail(organiser.email, subject, "generated email | do not respond", HTML).catch((err) => {
            console.log(err)
        })
    } else {
        console.log("email from organiser was not found in orderlist " + orderList.id)
    }
}

export function sendMailToUserRequestingPass(email) {

    let pass = generateRandomPassword(10)
    let HTML = createHTMLForTempPass(pass)

    if (email) {
        changePassword(pass, email)

        sendMail(email, "Your requested password", "generated email | do not respond", HTML).catch((err) => {
            console.log(err)
        })
    }
}