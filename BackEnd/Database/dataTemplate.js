/**
 * dataTemplate file
 * Created by Laurens Bittner and Thijs van der Vegt
 *
 * This file provides the keys of all entities.
 * These are used to validate an object.
 * The keys inside the object, are the required keys that should be provided, i.e. when POSTING a user,
 * you should provide the firstName, lastName, email and a password.
 */

let user = {
    firstName: "John",
    lastName: "Doe",
    email: "example@jcc.nl",
    password: "bcrypt hash",
}
// id : uuid
// roles

let orderlist = {
    title: "etentje",
    restaurantName: "McDonalds",
    linkToMenu: "google.nl het zelf ff",
    endDate: "date",
    invitedUsers: []
}
// hasEnded : boolean
// id : uuid
// organiser : uuid

let order = {
    description: "desc",
    orderlistID: "orderlist_uuid",
    paymentMethod: "Cash",
    notification: true,
    isPayed: false
}
// userID: "user_uuid"
// id : uuid
// isPayed : on creation = false

export const user_keys = Object.keys(user)
export const orderlist_keys = Object.keys(orderlist)
export const order_keys = Object.keys(order)