/**
 * Helper file for helper functions to read and write to files.
 * Created by Laurens Bittner and Thijs van der Vegt
 */

import fs from "fs"

/**
 * Function to write data to a file.
 * Will write the data to the corresponding .json file.
 *
 * @param fileName the file it should write to
 * @param object the data it should write
 * @returns {Promise<void>}
 */
export async function writeToFile (fileName, object) {
    const jsonString = JSON.stringify(object, null, 2);

    fs.writeFile(`Database/${fileName}.json`, jsonString, err => {
        if (err) {
            console.log('Error writing file', err)
        } else {
            console.log('Successfully wrote file')
        }
    })
}

/**
 * Function to read data from a file
 * Will read the data from an .json file
 * @param fileName
 * @returns {any}
 */
export  function readFromFile (fileName) {
    return parseJson(fs.readFileSync(`Database/${fileName}.json`, "utf8"))
}

/**
 * Helper function to parse the data
 * @param jsonString string to parse
 * @returns {any}
 */
function parseJson (jsonString) {
    try {
        return JSON.parse(jsonString);
    } catch (err) {
        console.log("Error parsing JSON string:", err);
    }
}