# READ ME

Voor het gebruiken van de webapp moet je kunnen inloggen. Hier staan de accounts gedocumenteerd die je kunt gebruiken om mee in te loggen.
Dit is vooral voor het admin account. Een User account kan je natuurlijk ook zelf aanmaken, maar we hebben er wel 1 gedocumenteerd.

Admin:

```
    "firstName": "JCC",
    "lastName": "Admin",
    "email": "Admin@JCCFoodStorage.com",
    "password": "Admin123"
 
``` 

User: 

```
    "firstName": "JCC",
    "lastName": "User",
    "email": "User@JCCFoodStorage.com",
    "password": "User123"
```
