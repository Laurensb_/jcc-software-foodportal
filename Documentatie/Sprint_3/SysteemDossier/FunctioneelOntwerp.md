# Functioneel Ontwerp Project Persistent

## Inhoudsopgave
1. Projectscope
    * Belanghebbenden
    * Doelen
    * Product(en)
    * Koppelvlakken
2. Risico's
3. Product overzicht
    * Korte omschrijving
    * Diagrammen
4. Requirements
5. Gebruikersplatform
    * Hardware
    * Software
    * Uitrol- of installatiehandleiding

DIT FUNCTIONEEL ONTWERP HEEFT ALS VOORNAAMSTE DOEL: <br/>
Beschrijving van hoe de applicatie moet functioneren en hoe dat er schematisch uitziet. Door dit goed vast te leggen weet het hele team waar het aan toe is en is er een gezamenlijke referentie.

## Project Scope 

### Belanghebbenden
Wie zijn er allemaal betrokken bij dit project?

| Persoon                     | Rol           |
| --------------------------- | ------------- | 
| JCC Software                | Opdrachtgever |
| Laurens Bittner             | Ontwikkelaar  |
| Roy Weghorst                | Ontwikkelaar  |
| Guus van der Vegt           | Ontwikkelaar  |
| Thijs van der Vegt          | Ontwikkelaar / Contact persoon  | 
|Frank van Doorn              | Begeleider |

### Doelen
Deze applicatie wordt gemaakt voor het project 'Project Persistent'. In dit project wordt er een bestelportaal gemaakt in opdracht van JCC Software. Voor de back end maken we gebruik van Node.Js en Express, en voor de front end gebruiken we Svelte. 

### Product(en)
De producten die opgeleverd dienen te worden:
1. De Back End.
2. De Front End.
3. Het Functioneel ontwerp (Dit bestand).
4. Het Technisch ontwerp.
5. De sprint planningen.
6. De product backlog. 
7. De sprint reviews. 
8. Overige documentatie (daily standups, urenverantwoording, etc.)

### Koppelvlakken
Dit project heeft een aantal raakvlakken met andere projecten / systemen. 
Er wordt geprogrammeerd met Node.Js en Express / Svelte, dus met eigenlijk alle projecten die op deze manier gemaakt zijn, zijn er raakvlakken. 
Verder wordt voor het onwikkelingsprocess de SCRUM methode gebruikt. Dit project heeft dus ook raakvlakken met projecten die ook ontwikkeld zijn met deze methode. Een voorbeeld hiervan is het project 'IT's in the Game' in kwartiel 2, van de opleiding HBO-ICT (Saxion).  

## Product Overzicht 

### Korte Omschrijving
Bij JCC Software worden er regelmatig verschillende lekkernijen bestelt, denk hierbij aan eten van de McDonalds of Subway, broodjes bij de Broodbode etc. Op dit moment loopt er altijd één collega met pen en papier langs alle collega’s om hun bestelling op te nemen. Verder moeten de collega’s ook nog betalen, de een doet dit liever contant en de ander liever via Tikkie. Dit allemaal handmatig doen is niet handig, en onoverzichtelijk. 
De opdracht is dus om een webapplicatie te ontwikkelen waarmee er een bestellijst kan worden aangemaakt en dat er collega’s hiervoor uitgenodigd kunnen worden. Die collega’s kunnen dan via de webapplicatie hun bestelling invoeren. 

#### Diagrammen

**De Login Pagina** <br>
![Login Page](../Images/PersistentLogin.png "Login Page")<br>

**De Registratie Pagina** <br>
![Registry Page](../Images/PersistentRegister.png "Registry Page")<br>

**Overzicht van de Bestellingen** <br>
![Bestellingen Overzicht](../Images/PersistentOverzicht.png "Bestellingen Overzicht")<br>

**Het aanmaken van een order** <br>
![Order Creation](../Images/PersistentCreateOrder.png "Create Order Page")<br>

**Het aanmaken van een bestellijst** <br>
![List Creation](../Images/PersistentCreateList.png "Create List Page")<br>

## Requirements 
De lijst van zaken waaraan het op te leveren product moet voldoen. 

| Nummer | Requirement | 
| ------ | ----------- | 
| 1.     | Als medewerker wil ik dat ik mezelf kan registreren met mijn email en wachtwoord zodat ik gebruik kan maken van het portaal. |
| 2.     | Als medewerker wil ik dat ik kan inloggen met mijn email en wachtwoord, zodat ik in mijn persoonlijke omgeving kom in het portaal. |
| 3.     | Als medewerker wil ik dat ik als organisator een bestellijst kan aanmaken zodat ik bestellingen van collega's kan opnemen. | 
| 4.     | Als medewerker wil ik een link naar de bestellijst in mijn email ontvangen, zodat ik aan de bestellijst kan deelnemen. |
| 5.     | Als medewerker wil ik binnen het portaal een overzicht met bestellijsten kunnen zien zodat ik gemakkelijk kan zien bij welke lijsten ik hoor. |
| 6.     | Als medewerker wil ik d.m.v. tekst mijn keuze kunnen doorgeven zodat mijn keuze in de bestellijst komt. |
| 7.     | Als medewerker wil ik de keuze hebben in betaalmethoden, zodat ik zelf de methode kan kiezen die mij het beste schikt. |
| 8.     | Als organisator wil ik dat wanneer de eindtijd van een bestellijst verstreken is, de keuzes naar mij gestuurd worden via de mail, zodat ik meteen kan bestellen. |
| 9.     | Als organisator van een bestellijst wil ik dat ik in de applicatie aan kan geven wie heeft betaald, zodat ik weet wie nog niet betaald heeft. |
| 10.    | Als organisator van een bestellijst wil ik dat ik binnen de applicatie de bestellingen van de leden kan zien, zodat ik weet wat ik aan de bedrijven moet doorgeven. |

## Gebruikersplatform 

### Hardware

Het is een vrij kleine applicatie, die je gewoon locaal zou moeten kunnen runnen op elke machine. Je hebt dus niet echt geadvanceerde hardware nodig. Het enige wat je echt nodig hebt is genoeg ruimte om het project te downloaden. 

De ontwikkelaars hebben de keuze om het project in twee IDE's te ontwikkelen. Namelijk IntelliJ IDEA en Visual Studio Code. Welke de ontwikkelaar gebruikt mag hij/zij zelf weten. De eis is wel dat de IDE zo is ingesteld dat het project zonder problemen ontwikkeld kan worden.

Verder hebben de IDE's wel enige systeemvereisten voor de hardware.

#### Systeemvereisten IntelliJ IDEA

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| RAM         | 2 GB free of RAM | 8 GB total system RAM |
| CPU         | Any modern CPU   | 	Multi-core CPU. IntelliJ IDEA supports multithreading for different operations and processes making it faster the more CPU cores it can use. | 
| Disk space  | 2.5 GB and another 1 GB for caches | SSD drive with at least 5 GB of free space |

(Source: [Jetbrains.com](https://www.jetbrains.com/help/idea/installation-guide.html#requirements))

#### Systeemvereisten Visual Studio Code
Visual Studio Code heeft weinig vereisten voor de hardware. De IDE kan op elke modere hardware draaien. Echter zijn er wel een aantal minimum vereisten voor de processor en RAM geheugen.

| Requirement | Minimum          | 
| ----------- | ---------------- |
| RAM         | 1GB of RAM |
| CPU         | 1.6 GHz or faster processor   |

(Source: [Requirements Visual Studio Code](https://code.visualstudio.com/docs/supporting/requirements))


### Software
Het project heeft een aantal requirements voor de software. Zo moet er software zijn die de IDE's ondersteunen maar ook zijn er besturingssystemen nodig. 

Onderstaand de besturingsystemen die vereist zijn voor de IDE's.

#### Software requirements voor IntelliJ IDEA: 

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| Windows | Windows 8 or newer | Latest 64-bit version of Windows |
| MacOs | MacOs 10.13 or newer | Latest 64-bit version of MacOs |
| Linux | Any Linux distribution that supports Gnome, KDE, or Unity DE. | Latest 64-bit version of Linux (for example, Debian, Ubuntu, or RHEL) |

(Source: [Jetbrains.com](https://www.jetbrains.com/help/idea/installation-guide.html#requirements))

#### Software requirements voor Visual Studio Code: 

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| Windows | Windows 7 or newer | Latest 64-bit version of Windows |
| MacOs | MacOs 10.11 or newer | Latest 64-bit version of MacOs |
| Linux | Any Linux distribution that supports Gnome, KDE, or Unity DE. | Latest 64-bit version of Linux (for example, Debian, Ubuntu, or RHEL) |

(Source: [Requirements Visual Studio Code](https://code.visualstudio.com/docs/supporting/requirements))

#### Overige software
Naast de besturingssystemen heb je ook nog een aantal andere software nodig. De meest belangrijke software zijn Node.Js en NPM.
Deze twee zijn benodigd om de backend draaiend te krijgen, en om het frontend framework te installeren.

Wij hebben gekozen om de backend in Node.Js i.c.m. Express te ontwikkelen. Om express te kunnen installeren is NPM vereist. Ook is NPM vereist om Svelte te installeren. Svelte is een framework, en wij hebben de keuze gemaakt om hiermee de frontend te ontwikkelen.


(Source: [Svelte](https://svelte.dev/))
(Source: [Express](https://expressjs.com/))
(Source: [NodeJs](https://nodejs.org/en/))
(Source: [NPM](https://www.npmjs.com/))


### Installatiehandleiding
- Voor dat de webapplicatie van deze git zou worden gepulled, is het eerst nodig om de meest recente node.js distributie te downloaden en instaleren van de [Node.js download pagina](https://nodejs.org/en/download/). Kies hier de distributie die hoort bij het operating system (bijv. Windows x64). 
- Na dat Node.js is geinstalleerd, kan nu het project van Git worden gepulled. 
- Na het pullen van de Gitlab repository, is het belangerijk dat in de console van het project `npm install` wordt uitgevoerd. Hiermee worden alle dependencies van het project geinstaleerd.
- Na het instaleren van de dependencies, kan de applicatie gestart worden met `npm run [placeholder]`


