# Sprint Backlog
In de sprint backlog worden alle User Stories getoont die waar wij in sprint 2 aan gaan werken. 
In de issues zelf zijn de 'How to test' en 'How to demo' te zien. (In de functionele issues).

## Definition of Done
Een User Story is 'done' wanneer het aan de volgende punten voldoet:
* De User Story is minimaal twee keer getest, door iemand anders dan degene die de User Story gemaakt heeft.
* De User Story is minimaal 1 keer goed door de test heen gekomen. 
* De User Story is ook gecontroleerd / er is feedback gegeven door iemand anders uit het team.
* De User Story is geclosed op GitLab. 

## Issues
![1e Gedeelte lijst issues](../Images/userStories1.PNG "Issues")<br>
![2e Gedeelte lijst issues](../Images/userStories2.PNG "Issues")<br>
![3e Gedeelte lijst issues](../Images/userStories3.PNG "Issues")<br>
![4e Gedeelte lijst issues](../Images/userStories4.PNG "Issues")<br>
![5e Gedeelte lijst issues](../Images/userStories5.PNG "Issues")<br>
