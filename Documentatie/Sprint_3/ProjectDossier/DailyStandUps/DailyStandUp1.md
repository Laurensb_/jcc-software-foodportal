# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 12.10.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| 1e dag sprint, geen taken                 |               |                 |             |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Issues aanmaken voor sprint 3                   | Iedereen      |             |
| Front-end (Aanpassen Orderlist)                 | Roy           |             |
| Front-end (Feedback sprint review verwerken)    | Guus          |             |
| Nieuwe emails maken                             | Laurens       |             |
| Front-end (Aanmaken Order)                      | Thijs         |             |
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|    /                           |            |              |

<br/> 




