# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 13.10.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Issues aanmaken voor sprint 3             | Iedereen      | Ja              | Issues staan in GL |
| Front-end (Aanpassen Orderlist)           | Roy           | Begonnen        | Kleine problemen, (nog) geen hulp nodig |
| Front-end (Feedback sprint review verwerken) | Guus       | deels           | |
| Nieuwe emails maken                       | Laurens       | Ja              | |
| Front-end (Aan maken Order)               | Thijs         | deels           | Component gemaakt / functioneel / evt stijl nog aanpassen |


<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Verder werken aan de 'change orderlist' pagina  | Roy           |             |
| Sessionstorage omzetten naar localstorage       | Guus          | Als je via een email naar de site ging, herkende hij de token niet meer -> fixen|
| Emails implementeren en wachtwoorden laten genereren | Laurens | |
| Verder werken aan 'create order' component      | Thijs         |             | 
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|    /                           |            |              |

<br/> 





