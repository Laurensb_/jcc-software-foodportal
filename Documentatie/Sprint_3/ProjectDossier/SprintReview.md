# Sprint 3 Review

### Doel Sprint: Het afronden van de applicatie en eventuele could haves toevoegen aan de applicatie
---

### Onderwerpen:
1. De applicatie moet een detail pagina hebben van de gebruiker
2. De applicatie moet een detail en edit pagina hebben voor orderlists
3. De applicatie moet orders op kunnen nemen, deze orders moeten ook verweiderd of aangepast kunnen worden
4. De applicatie moet een pagina hebben waar de gebruiker zijn/haar details kan aanpassen

---

### User Stories:  <br/>
#### Afgeronde User Stories:
![Eerste afbeelding user stories](../Images/Sprint3UserStories1.png)
![Tweede afbeelding user stories](../Images/Sprint3UserStories2.png)
![Derde  afbeelding user stories](../Images/sprint2UserStories3.PNG)

#### Onafgeronde User Stories:
Er zijn geen userstories niet afgerond deze sprint

---

### Review:
Tijdens deze sprint was het doel om de applicatie af te ronden. Dit betekende dat er nog een paar pagina's moesten worden gemaakt. Maar deze kregen wij optijd deze sprint af. Ook waren er nog een paar could haves die we hebben weten te implementeren (bijv. de Admin user account).

Verder kregen wij  ook deze sprint alle user stories op tijd af en zijn deze ook getest. Voor de rest is deze sprint soepel verlopen en is er niet iets speciaals te melden

---

### Burn-down chart:

![Burn-down chart afbeelding](../Images/Sprint3BurnDownChart.png)

De Burn-down chart van deze sprint ziet er een beetje apart uit. Voornamenlijk in het begin lijkt de lijn stil te staan, dit komt omdat er tussen de eerste en tweede week van de sprint een vakantie zat, waar dus weinig werkelijk gedaan werdt. Verder zijn alle issues alweer iets voor de deadline gesloten, dit komt omdat we alles af wouden hebben voor de presentatie op donderdag terwijl de deadline zich op zondag bevindt.

---

### Focus Factor
De focus factor wordt berekend door de velocity te delen door het aantal gemaakte uren.
Het aantal beschikbare uren zijn 50 uren per week. Sprint 3 bestond uit 2 weken, dus hadden we 100 beschikbare uren. Hiervan hebben wij er 84.38 gemaakt. 

De velocity is hoeveel Storypoints we in deze sprint hebben behandeld, dit zijn er in totaal 95. 

Dus -> "Velocity" / "Gebruikte uren" --> 95/100.93 = 0.94

Dus onze focus factor voor de sprint was 0.94, dit getal is veel hoger dan vorige sprint, dit wil dus zeggen dat we veel efficiënter zijn gaan werken.

In de vorige sprint was er berekend dat wij deze sprint ongeveer 64 storypoint aan zouden kunnen. Hier zijn wij duidelijk ver over heen gegaan, wat te verklaren is door de zeer goede samenwerking van ons team en het feit dat we steeds meer over svelte en express hebben geleerd.
<br/>





