# Sprint Planning

De volgende user stories gaan in de 2e sprint worden uitgewerkt:<br>
![1e Gedeelte lijst issues](../Images/userStories1.PNG "Issues")<br>
![2e Gedeelte lijst issues](../Images/userStories2.PNG "Issues")<br>
![3e Gedeelte lijst issues](../Images/userStories3.PNG "Issues")<br>
![4e Gedeelte lijst issues](../Images/userStories4.PNG "Issues")<br>
![5e Gedeelte lijst issues](../Images/userStories5.PNG "Issues")<br>

## Deadline
Voor elke sprint worden ongeveer 2 weken gerekend. Voor sprint 2 zal de deadline dan op **31-10-2021** liggen. Zoals beschreven in de werkafspraken is er ruimte voor 24 uur uitloop, mits dit overlegt is binnen het team. Deze uitloop is alleen bedoelt voor werk die door onvoorziene omstandigheden niet uitgevoerd had kunnen worden.

## Taakverdeling
Hieronder is globaal beschreven wat de taken per teamlid zijn. Ook worden de userstories die dit gaat afwerken vermeld.

### Laurens 
- Het maken van een overzicht pagina voor persoonsgegevens
- Het maken van een aanpassings pagina voor persoonsgegevens
- Het maken van een 'wachtwoord vergeten' pagina

**Userstories:**
\#65, \#70, \#71, \#72, \#73

### Thijs
- Het maken van een create pagina voor orders
- Het maken van een anpassings pagina voor orders

**Userstories:**
\#9, \#64

### Roy
- Het maken van een pagina voor het aanpassen van orderlists

**Userstories:**
\#51, \#61, \#63

### Guus
- De aanpassingen invoeren voorgesteld door JCC Software
- Het maken van de detail pagina voor orderlists
- Kleine veranderingen storage methode frontend

**Userstories:**
\#57, \#59, \#62, \#66, \#67, \#69, \#74, \#75, \#77, \#79, \#80

## Doel Sprint 3
Het doel van de 3de sprint is het maken van de pagina's voor de details van gebruikers, orders en orderlists. Ook worden de aanpassings pagina's van al deze items gemaakt. Verder zal er nog gewerkt worden aan enige nice to haves en/of bugfixes van problemen die zich voor kunnen doen in deze sprint

## Eindproduct
Het eindproduct van deze sprint is een compleet functionele applicatie voor JCC Software. Alle pagina's moeten compleet functioneren en zo veel mogelijke nice to haves zullen zijn uitgewerkt.

