# Sprint Retrospective

## Verbeteringen van vorige keer:
* Er is gecontroleerd op progressie tot het eind doel, iedereen werkte goed door en er is dus niemand op aangesproken
* Als er een onderdeel van het project afgerond werdt, werd dit direct gemeld in de groepschat, waardoor een ander teamgenoot het sneller kon inspecteren op eventuele fouten

## Goede punten:
* De afgeronde user stories waren ook deze sprint weer van goede kwaliteit.
* Iedereen is zijn afspraken nagekomen, het werk was goed gemaakt en werdt op tijd ingeleverd
* Feedback was ook weer goed gehanteerd, zodra iets niet goed was werdt degene die het gemaakt had hierop aangesproken

## Minder goede punten:
* Taakverdeling, door de redelijk weinige hoeveelheid werk dat over was deze sprint was de taakverdeling deze sprint iets minder. 

## Verbeteringen
* Omdat er geen nieuwe sprint is, sinds sprint 3 de laatste sprint van dit project was. Zijn er geen verbeteringen te noemen voor de volgende sprint. Wel kunnen er verbeteringen worden meegenomen voor een mogelijk volgend project dat deze groep misschien nog gaat doen
* Communicatie, hoewel goed aan het eind, kon verbeterd worden in het begin. Zorg er voor dat het duidelijk is aan iedereen wat er gebeurd en wie waar mee bezig is. Ook welke mogelijk eisen er worden gestelt en deadlines die worden gezet.
* Zorg er voor dat iedereen zich comfortabel voelt binnen de project groep. Probeer niet kleinere groepen binnen een project groep te vormen.
* Iedereen moet inzet tonen, laat mensen niet achter omdat ze mogelijk niet zo veel werk opleveren. In plaats daar van, probeer te kijken waar het mis gaat en welke oplossingen er mogelijk zijn
