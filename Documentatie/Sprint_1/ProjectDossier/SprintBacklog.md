# Sprint Backlog
In de sprint backlog worden alle User Stories getoont die waar wij in sprint 1 aan gaan werken. 
In de issues zelf zijn de 'How to test' en 'How to demo' te zien. (In de functionele issues).

## Definition of Done
Een User Story is 'done' wanneer het aan de volgende punten voldoet:
* De User Story is minimaal twee keer getest, door iemand anders dan degene die de User Story gemaakt heeft.
* De User Story is minimaal 1 keer goed door de test heen gekomen. 
* De User Story is ook gecontroleerd / er is feedback gegeven door iemand anders uit het team.
* De User Story is geclosed op GitLab. 

## Issues
![Eerste afbeelding user stories](../Images/image_2021-09-24_142902.png)
![Tweede afbeelding user stories](../Images/image_2021-09-24_143357.png)
![Derde  afbeelding user stories](../Images/image_2021-09-24_143500.png)
![Vierde afbeelding user stories](../Images/image_2021-09-24_143729.png)
