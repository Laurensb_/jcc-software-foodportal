# Sprint 1 Review

### Doel Sprint: Het maken van de backend, registratie, login en documentatie voor de applicatie
---

### Onderwerpen:
1. De applicatie moet een werkende backend hebben
2. De applicatie moet objecten kunnen opslaan in een form van data opslag
3. De applicatie moet een werkende registratie pagina hebben
4. De applicatie meot een werkende login pagina hebben

---

### User Stories:  <br/>
#### Afgeronde User Stories:
![Eerste afbeelding user stories](../Images/image_2021-09-24_142902.png)
![Tweede afbeelding user stories](../Images/image_2021-09-24_143357.png)
![Derde  afbeelding user stories](../Images/image_2021-09-24_143500.png)

#### Onafgeronde User Stories:
![Vierde afbeelding user stories](../Images/image_2021-09-24_143729.png)

Zoals te zien is is er deze sprint èèn user story niet compleet afgerond. Dit is veroorzaakt door een vertraging die is opgelopen tijdens het maken van de database, en zal dus met de volgende sprint worden opgenomen.

---

### Review:
Tijdens deze sprint was het doel het opzetten van de database, en het maken van de pagina's voor zowel inloggen als registreren. <br>

Bij het maken van de database voor de back-end van de applicatie waren er veel problemen verschenen tijdens het proberen te migreren van de database tussen de verschillende systemen van de programmeurs. Deze problemen plus het feit dat de applicatie aan de kleine kant is voor een complete database. Heeft geresulteerd in de keuze om over te stappen op een JSON bestand in plaats van een complete database. 

Zowel de registratie pagina als de inlog pagina zijn wel beide afgekomen en compleet. Hier waren voor de rest geen grote problemen.

---

### Burn-down chart:

![Burn-down chart afbeelding](../Images/image_2021-09-24_144943.png)

De burn-down chart van deze sprint ziet er apart uit. Dit komt door een kleine technische error met het toevoegen van user stories aan de sprint. We waren in het begin namelijk vergeten de user stories aan de milestone te koppelen. En zal dus ook niet als accuraat moeten worden beschouwd

---

### Focus Factor
De focus factor wordt berekend door de velocity te delen door het aantal beschikbare uren.
Het aantal beschikbare uren zijn 50 uren per week. Sprint 1 bestond uit 2 weken, dus hadden we 100 beschikbare uren.

De velocity is hoeveel Storypoints we in deze sprint hebben behandeld, dit zijn er in totaal 42. 

Dus -> "Velocity" / "Beschikbare uren" --> 42/78.85 = 0.53

Dus onze focus factor voor de sprint was 0.53, dit wil voor deze sprint nog niet zo heel veel zeggen. Maar als het goed is wordt dit getal per sprint hoger, wat betekent dat we efficiënter zijn gaan werken.

We kunnen met dit getal wel het aantal story points die wij voor de volgende sprint aan zouden moeten kunnen berekenen. 
Dat is namelijk -> 0.53 x 100 = 53. dit betekent dat wij in sprint 2 ongeveer 53 storypoints aankunnen. 


