# Sprint Retrospective

## Goede punten:
* Goede taakverdeling, iedereen wist hij moest doen.
* Nakomen van afspraken, iedereen had zijn werk op tijd af, en het was op een serieuze manier gemaakt.
* Kwaliteit, naar onze mening is het product wat opgeleverd is, van goede kwaliteit. 

## Minder goede punten:
* Tijdsplanning, het was duidelijk wanneer de deadline was, maar de planning naar die deadline toe was niet goed.
* Feedback op elkaars werk, soms wanneer iemand iets af had, dan geloofde de rest het wel. Dit zorgde er soms voor dat bepaalde dingen niet werkten / helemaal goed waren.
* Communicatie, de communicatie verliep niet altijd even goed. Sommige dingen werden weinig gecommuniceerd en van andere dingen werd er verwacht dat het helemaal punt voor punt gecommuniceerd werd.

## Verbeteringen
* Om voor een betere tijdsplanning te zorgen, gaan we volgende sprint de grotere taken ook opdelen in kleinere stukken, en voor die aparte stukken deadlines bepalen. 
* Om voor betere feedback te zorgen, gaan we volgende sprint het werk van anderen beter 'controleren' / nalezen. Op deze manier kunnen we sommige fouten er beter uithalen.
* Qua communicatie, gaan we uit van zelfstandigheid. Wanneer iemand een taak krijgt is het niet de bedoeling dat een ander teamgenoot punt voor punt uit moet leggen wat er precies van diegene verwacht wordt. Wanneer iemand er echt niet uitkomt is het natuurlijk geen probleem, maar iedereen heeft zijn eigen werk waar ze druk genoeg mee zijn. 
