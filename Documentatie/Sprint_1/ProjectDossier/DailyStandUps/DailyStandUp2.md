# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 09.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| User Stories aanmaken in GitLab (Product backlog) | Guus | Deels | Was nog niet helemaal klaar |
| PVA: Opzet bestand / template | Thijs | Ja | |
| PVA: Voorwoord | Thijs | Ja | |
| PVA: Inleiding | Thijs | Ja | |
| PVA: Probleemstelling | Thijs | Ja | |
| PVA: Doelstelling | Roy | Ja | |
| PVA: Ontwerpkeuzes | Laurens | Ja |
| PVA: Werkplanning | Laurens | Deels | Was nog niet helemaal klaar |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Product backlog afmaken in Gitlab               | Guus / Laurens|             |
| Product backlog prioriteren                     | Iedereen      | We moeten allemaal helpen met het inschatten van de story points |
| PVA: Testing                                    | Roy           |             |
| PVA: Theoretisch kader                          | Thijs         |             |
| PVA verbeteren waar mogelijk                    | Iedereen      | Als iemand iets ziet wat beter kan, of ziet dat er nog iets mist, dan kan dit aangepast worden | 
| PVA opsturen naar de klant                      | Thijs         |             |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 
