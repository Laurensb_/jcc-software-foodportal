# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 25.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Back-end maken                            | Laurens/Thijs | Ja              | Back-end klaar voor de issues van sprint 1 |
| Verder met login page                     | Guus          | Ja               | |
| Verder met registry page                  | Roy           | Ja              | |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Ervoor zorgen dat de back-end klaar is voor inlevering | Laurens / Thijs | |
| Ervoor zorgen dat de login pagina klaar is voor inlevering | Guus | |
| Ervoor zorgen dat de registry pagina klaar is voor inlevering | Roy | |
| Ervoor zorgen dat gitlab klaar is voor inlevering | Guus | |
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 




