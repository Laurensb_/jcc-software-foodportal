# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 21.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Opzetten Database                         | Laurens / Thijs | Ja            | Implementatie ging niet helemaal goed, we switchen naar een andere oplossing. | 
| Maken login page + implementatie          | Guus            | Ja            | page gemaakt, implemenatie niet. Komt door de database |
| Maken registratie page + implementatie    | Roy             | Nee           | Database config lukte niet. |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| BackEnd maken                                   | Laurens / Thijs | |
| Verder met login page                           | Guus          | |
| Verder met registry page                        | Roy           | |
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 



