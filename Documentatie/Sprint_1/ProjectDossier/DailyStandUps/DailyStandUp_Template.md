# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | xx.xx.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
|                                           |               |                 |             |
|                                           |               |                 |             |
|                                           |               |                 |             |
|                                           |               |                 |             |
|                                           |               |                 |             |
|                                           |               |                 |             |
<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
|                                                 |               |             |
|                                                 |               |             |
|                                                 |               |             |
|                                                 |               |             |
|                                                 |               |             |
|                                                 |               |             |
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|                                |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 



