# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 14.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Product backlog afmaking in Gitlab        | Guus / Laurens| Ja              | Alleen Must Haves, nog niet de nice to haves. |
| Product backlog prioriteren               | Iedereen      | Ja              | |
| PVA: Testing                              | Roy           | Ja              | |
| PVA: Theoretisch kader                    | Thijs         | Ja              | |
| PVA verbeteren waar mogelijk              | Iedereen      | Ja              | De puntjes zijn op de i gezet. |
| PVA opsturen naar de klant                | Thijs         | Ja              | Helaas geen reactie terug gehad.| 

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| FO: Wireframes                                  | Laurens       |             |
| Functioneel Ontwerp                             | Thijs         |             | 
| Technisch Ontwerp                               | Guus / Roy    | Zij verdelen onderling de taken. | 
| Feedback PVA Verwerken                          | Thijs         | Na het inleveren hebben we feedback gekregen. | 
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 

