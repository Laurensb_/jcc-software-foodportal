# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 16.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| FO: Wireframes                            | Laurens       | Deels           | Nog niet alle wireframes af. |
| Functioneel Ontwerp                       | Thijs         | Deels           | De wireframes moeten er nog in, en nog even gecontroleerd worden. |
| Technisch Ontwerp                         | Guus / Roy    | Deels           | Nog niet alle kopjes zijn af |
| Feedback PVA verwerken                    | Thijs         | Nee             | Geen feedback ontvangen. | 

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| FO afmaken                                      | Thijs / Laurens | |
| TO afmaken                                      | Guus / Roy      | | 
| FO / TO opsturen naar JCC                       | Thijs           | |
<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 


