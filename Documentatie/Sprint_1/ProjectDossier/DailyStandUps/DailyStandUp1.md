# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 07.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Eerste Standup, dus nog geen taken        |               |                 |             |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| User Stories aanmaken in GitLab (Product backlog) | Guus | Hoeft nog niet helemaal in GitLab, maar wel in het PVA.|
| PVA: Opzet bestand / template | Thijs | |
| PVA: Voorwoord | Thijs | |
| PVA: Inleiding | Thijs | |
| PVA: Probleemstelling | Thijs | |
| PVA: Doelstelling | Roy | |
| PVA: Ontwerpkeuzes | Laurens | |
| PVA: Werkplanning | Laurens | |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|                                |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 




