# Sprint Planning

De volgende user stories gaan in de 1e sprint worden uitgewerkt:
![1e Gedeelte lijst issues](../Images/Issues1.png "Issues")<br>
![2e Gedeelte lijst issues](../Images/Issues2.png "Issues")<br>
![3e Gedeelte lijst issues](../Images/Issues3.png "Issues")<br>

## Deadline
Voor elke sprint worden ongeveer 2 weken gerekend. Voor sprint 1 zal de deadline dan op **26-09-2021** liggen. Zoals beschreven in de werkafspraken is er ruimte voor 24 uur uitloop, mits dit overlegt is binnen het team. Deze uitloop is alleen bedoelt voor werk die door onvoorziene omstandigheden niet uitgevoerd had kunnen worden.

## Taakverdeling
Hieronder is globaal beschreven wat de taken per teamlid zijn. Ook worden de userstories die dit gaat afwerken vermeld.

### Laurens 
- Het opzetten van de backend
- Het creeeren van middleware
- Het creeeren van de controllers
- Het opzetten van een opslagstructuur (JSON)

**Userstories:**
\#20, \#22, \#23, \#25, \#26, \#27, \#28, \#29, \#35, \#36


### Thijs
- Het opzetten van de backend
- Het creeeren van de routers
- Het creeeren van middleware
- Het creeeren van de controllers

**Userstories:**
\#20, \#23, \#24, \#25, \#26, \#27, \#30, \#33,  \#35, \#36

### Roy
- Het opzetten van het registratie formulier (frontend, html, css)
- Het versturen van een call om de user aan te maken naar de backend
- Het opslaan van de JWT token in de user store.
- Het doorsturen naar de home pagina, na een succesvolle registratie.
- Het tonen van eventuele errors.

**Userstories:**
\#1, \#39, \#40

### Guus
- Het opzetten van de frontend
- Het opzetten van het login formulier (frontend, html, css)
- Het versturen van een call om de user in te loggen naar de backend
- Het opslaan van de JWT token in de user store. Deze komt terug als response uit de backend.
- Het doorsturen naar de home pagina, na een succesvolle inlog poging.
- Het tonen van eventuele errors.

**Userstories:**
\#2, \#21, \#37, \#38 

## Doel Sprint 1
Het doel van de 1e sprint is om het portaal te beveiligen met authenticatie. Medewerkers moeten met hun email en wachtwoord kunnen inloggen en toegang krijgen tot het portaal. Verder is het ook de bedoeling dat medewerkers voor de eerste keer een account aan kunnen maken.

Dit zal worden gedaan door middel van routes in de backend, en deze te verbinden met de frontend. De frontend zal een request maken met de inloggegevens, en de backend zal deze valideren. Op basis van de validatie zal er of een JWT token worden teruggestuurd, of een foutmelding met de corresponderende fouten. Deze fouten zullen dan weer worden afgehandeld en worden weergegeven op de frontend.


## Authenticatie
In deze sprint zal de authenticatie voor het portaal ontwikkeld worden. Dit gedeelte zal worden opgesplitst worden in twee helften, de frontend en de backend. De backend is verantwoordelijk voor het controleren van de gegevens, het verstrekken van de JWT tokens en het valideren hiervan. Ook zal de backend de gebruiker aanmaken, en het wachtwoord hashen door middel van bcrypt.

Ook zullen gebruikers een account moeten aanmaken als ze van het portaal gebruik willen maken. Zo kunnen bestellingen en bestellijsten kunnen worden gekoppeld aan een persoon. Maar ook zodat niet elk willekeurig persoon toegang heeft tot de applicatie. Een gebruiker kan een account aanmaken met een email adres en wachtwoord. Hiermee kunnen zij dan ook weer inloggen.

## Foutmeldingen
Wanneer er fouten optreden zal dit ook afgehandeld moeten worden. De backend zal fouten door middel van tekst en corresponderende status codes terug koppelen naar de frontend. De frontend zal met deze fouten een melding tonen. De tekst die in de melding uit de backend komt, zal hierin getoont worden. Deze meldingen zullen dan boven het inlogformulier getoont worden.

## Eindproduct
Het eindproduct is een login en registratie pagina. Tevens zijn de benodigde endpoints en functionaliteit in de backend ontwikkeld en beschikbaar voor de frontend.

