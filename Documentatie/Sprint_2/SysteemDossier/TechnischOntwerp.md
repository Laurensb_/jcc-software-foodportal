# Technisch Ontwerp Project Persistent

## Inhoudsopgave
1. Ontwikkelplatform
    * Hardware
    * Software
    * Installatiehandleiding
2. Modellen
    * Basic modellen
    * Overige diagrammen
3. Testplan
    * Acceptatiecriteria
    * Ontwerpkeuzes

DIT TECHNISCH ONTWERP HEEFT ALS VOORNAAMSTE DOEL: <br/>
Het overdraagbaar maken van dit project aan een nieuw teamlid dan wel
dit gehele project over te kunnen dragen aan een nieuw team
(om bijv. na enige tijd het project verder te kunnen ontwikkelen)

## Ontwikkelplatform

### Hardware
De applicatie is niet heel heftig, je hebt dus niet echt geadvanceerde hardware nodig. Het enige wat je echt nodig hebt is genoeg ruimte om het project te downloaden. 

De ontwikkelaars hebben de keuze om het project in twee IDE's te ontwikkelen. Namelijk IntelliJ IDEA en Visual Studio Code. Welke de ontwikkelaar gebruikt mag hij/zij zelf weten. De eis is wel dat de IDE zo is ingesteld dat het project zonder problemen ontwikkeld kan worden.

Verder hebben de IDE's wel enige systeemvereisten voor de hardware.

#### Systeemvereisten IntelliJ IDEA

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| RAM         | 2 GB free of RAM | 8 GB total system RAM |
| CPU         | Any modern CPU   | 	Multi-core CPU. IntelliJ IDEA supports multithreading for different operations and processes making it faster the more CPU cores it can use. | 
| Disk space  | 2.5 GB and another 1 GB for caches | SSD drive with at least 5 GB of free space |

(Source: [Jetbrains.com](https://www.jetbrains.com/help/idea/installation-guide.html#requirements))

#### Systeemvereisten Visual Studio Code
Visual Studio Code heeft weinig vereisten voor de hardware. De IDE kan op elke modere hardware draaien. Echter zijn er wel een aantal minimum vereisten voor de processor en RAM geheugen.

| Requirement | Minimum          | 
| ----------- | ---------------- |
| RAM         | 1GB of RAM |
| CPU         | 1.6 GHz or faster processor   |

(Source: [Requirements Visual Studio Code](https://code.visualstudio.com/docs/supporting/requirements))


### Software
Het project heeft een aantal requirements voor de software. Zo moet er software zijn die de IDE's ondersteunen maar ook zijn er besturingssystemen nodig. 

Onderstaand de besturingsystemen die vereist zijn voor de IDE's.

#### Software requirements voor IntelliJ IDEA: 

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| Windows | Windows 8 or newer | Latest 64-bit version of Windows |
| MacOs | MacOs 10.13 or newer | Latest 64-bit version of MacOs |
| Linux | Any Linux distribution that supports Gnome, KDE, or Unity DE. | Latest 64-bit version of Linux (for example, Debian, Ubuntu, or RHEL) |

(Source: [Jetbrains.com](https://www.jetbrains.com/help/idea/installation-guide.html#requirements))

#### Software requirements voor Visual Studio Code: 

| Requirement | Minimum          | Recommended           |
| ----------- | ---------------- | --------------------- |
| Windows | Windows 7 or newer | Latest 64-bit version of Windows |
| MacOs | MacOs 10.11 or newer | Latest 64-bit version of MacOs |
| Linux | Any Linux distribution that supports Gnome, KDE, or Unity DE. | Latest 64-bit version of Linux (for example, Debian, Ubuntu, or RHEL) |

(Source: [Requirements Visual Studio Code](https://code.visualstudio.com/docs/supporting/requirements))

#### Overige software
Naast de besturingssystemen heb je ook nog een aantal andere software nodig. De meest belangrijke software zijn Node.Js en NPM.
Deze twee zijn benodigd om de backend draaiend te krijgen, en om het frontend framework te installeren.

Wij hebben gekozen om de backend in Node.Js i.c.m. Express te ontwikkelen. Om express te kunnen installeren is NPM vereist. Ook is NPM vereist om Svelte te installeren. Svelte is een framework, en wij hebben de keuze gemaakt om hiermee de frontend te ontwikkelen.


(Source: [Svelte](https://svelte.dev/))
(Source: [Express](https://expressjs.com/))
(Source: [NodeJs](https://nodejs.org/en/))
(Source: [NPM](https://www.npmjs.com/))


### Installatiehandleiding
- Voor dat de webapplicatie van deze git zou worden gepulled, is het eerst nodig om de meest recente node.js distributie te downloaden en instaleren van de [Node.js download pagina](https://nodejs.org/en/download/). Kies hier de distributie die hoort bij het operating system (bijv. Windows x64). 
- Na dat Node.js is geinstalleerd, kan nu het project van Git worden gepulled. 
- Na het pullen van de Gitlab repository, is het belangerijk dat in de console van het project `npm install` wordt uitgevoerd. Hiermee worden alle dependencies van het project geinstaleerd.
- Na het instaleren van de dependencies, kan de applicatie gestart worden met `npm run start`

## Testplan 
Bij het maken van een User Story zal er een ‘how to test’ worden toegevoegd. Deze ‘how to test’s worden gehanteerd tijdens het testen als een testplan. 

Tijdens het ontwerpen en verwerkelijken van deze web-applicatie zal gebruik worden gemaakt van user testing. Deze tests zullen worden uitgevoerd door één of meerdere teamleden van het scrum team. Waar de vooraf gemaakte testplannen zullen worden doorgelopen door de tester om te verifiëren dat de gewenste uitkomst ook de werkelijke uitkomst is.  

Ook worden er voor de back-end .rest files aangemaakt, waar elke verschillende call wordt getest. Good case en bad case. Wanneer al deze testen slagen (dat weet je automatisch door die files), weet je dat de back-end goed werkt. 

Verder wordt tijdens het testen ook gekeken of de keuzes in front-end design ook makkelijk handelbaar en niet onnodig lastig zijn om te gebruiken 

### Acceptatiecriteria

| Nummer | Requirement | Acceptatietest
| ------ | ----------- | --------------
| 1.     | Als medewerker wil ik dat ik mezelf kan registreren met mijn email en wachtwoord zodat ik gebruik kan maken van het portaal.| Een gebruiker moet kunnen registreren
| 2.     | Als medewerker wil ik dat ik kan inloggen met mijn email en wachtwoord, zodat ik in mijn persoonlijke omgeving kom in het portaal. | een geregistreerde gebruiker moet kunnen inloggen
| 3.     | Als medewerker wil ik dat ik als organisator een bestellijst kan aanmaken zodat ik bestellingen van collega's kan opnemen.  | gebruiker moet bestellijst kunnen aanmaken en andere gebruikers uitnodigen
| 4.     | Als medewerker wil ik een link naar de bestellijst in mijn email ontvangen, zodat ik aan de bestellijst kan deelnemen. | gebruiker moet email krijgen als ze worden uitgenodigd voor bestellijst 
| 5.     | Als medewerker wil ik binnen het portaal een overzicht met bestellijsten kunnen zien zodat ik gemakkelijk kan zien bij welke lijsten ik hoor. | gebruiker moet overzicht van alle uitgenodigde bestellijsten kunnen zien
| 6.     | Als medewerker wil ik d.m.v. tekst mijn keuze kunnen doorgeven zodat mijn keuze in de bestellijst komt. | gebruiker moet keuze voor bestelling kunnen opgeven
| 7.     | Als medewerker wil ik de keuze hebben in betaalmethoden, zodat ik zelf de methode kan kiezen die mij het beste schikt. | gebruiker moet kunnen kiezen tussen tikkie of contant betalen
| 8.     | Als organisator wil ik dat wanneer de eindtijd van een bestellijst verstreken is, de keuzes naar mij gestuurd worden via de mail, zodat ik meteen kan bestellen. | organisator moet mail van ingevulde bestellijst krijgen na dat de lijst is ingevuld
| 9.     | Als organisator van een bestellijst wil ik dat ik in de applicatie aan kan geven wie heeft betaald, zodat ik weet wie nog niet betaald heeft. | organisator moet kunnen aangeven wie heeft betaald
| 10.    | Als organisator van een bestellijst wil ik dat ik binnen de applicatie de bestellingen van de leden kan zien, zodat ik weet wat ik aan de bedrijven moet doorgeven. | organisator moet ingevulde bestellijst kunnen uitlezen in applicatie

### Ontwerpkeuzes

#### Front-end
Voor het front-end gedeelte van onze applicatie, hebben we ervoor gekozen om het te gaan ontwikkelen met behulp van het framework “svelte”. Wij hebben hier als groep voor gekozen, omdat een tal van ons met een ander project bezig is, waar we ook svelte voor het front-end van de applicatie gebruiken. Omdat dit het geval is, vond ieder van ons het wel handig om voor dit project dan ook svelte te gaan gebruiken. Dit zorgt er dan voor dat ons proces iets makkelijker verloopt en uiteindelijk ook voor een beter product. 

#### Back-end
Voor het back-end hadden we een paar keuzes openstaan. Ons groepje heeft te maken gehad met een webserver laten lopen via Java met spring boot en ook via node.js met express. We moesten dus een keuze tussen een van de twee gaan maken. We hebben er niet voor gekozen om nieuwe technieken op te zoeken, omdat we graag met iets willen werken wat bekend is.  

Aangezien we javascript in de front-end gaan gerbuiken, hebben we ervoor gekozen om node.js te gaan gebruiken, gewoon om de programmeer talen op een lijn te houden. 

Het back-end wordt dan gekoppeld aan een database met postgres, ook weer omdat we al ervaring hebben met het gebruik van databases in postgres. 

#### Kleuren
Er is voor gekozen om de gegeven huiskleuren van JCC Software te hanteren, dit is sinds the applicatie specifiek voor dit bedrijf is gemaakt. 

