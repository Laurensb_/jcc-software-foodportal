De backend en alle, aan de backend gerelateerde, issues worden getest d.m.v. de .rest files, die in de backend te vinden zijn. Hiermee sturen we een test request en kijken op basis van bepaalde gegevens of de test geslaagd is ja of nee. 

De Test Results zijn .html files van de test results van de .rest files in de backend. 

**Open deze files in je browser om ze te bekijken!**
