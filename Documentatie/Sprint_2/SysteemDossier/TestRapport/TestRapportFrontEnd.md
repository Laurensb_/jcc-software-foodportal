# Testplan

## Frontend
Voor de frontend zullen de volgende tests worden uitgewerkt:
- Het inloggen en registreren
- De functionaliteit van de bestellijst overzicht pagina (ophalen uit de backend, pagination, filters, etc.)
- Het aanmaken van een bestellijst (versturen naar de backend, velden, etc.)

### Resultaten
Onderstaand zijn de resultaten in een tabel beschreven.

| Omschrijving | Issues | Geslaagd (Ja/Nee) | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Het testen van van het inloggen. Wordt de data goed gevalideerd, wordt de data goed meegestuurd, komt er een valide JWT token terug en wordt deze opgeslagen in de session storage.| #2, #21, #37, #38 | Ja | De test is geslaagd. Het toevoegen van de jwt token aan de store is pas naderhand toegevoegd, zodat je na een refresh nog bent ingelogd|
| Het testen van ophalen en tonen van de bestellijsten uit de backend. De data moet worden opgeslagen in een store en deze moet in een svelte component door middel van een loop worden ingeladen. (Kaarten worden aangemaakt) | #7, #47 | Ja | De data wordt opgeslagen in de store en getoont in het component. Alleen word er in de tussentijd nog wat met de data gedaan (filteren, pagination) |
| Het testen van het opdelen in verschillende pagina's. De bestellijsten worden opgedeeld in pagina's van maximaal 12 per pagina. Hiervoor moet er een navigatie zijn en moeten de orginele lijsten worden opgedeeld. | #52 | Ja | De lijsten zijn opgedeeld en er zijn verschillende pagina's op basis van het aantal lijsten. Ook is er een overgangstransitie. | 
| Het testen van verschillende filters op de bestellijsten. Zo moet er gezocht kunnen worden op restaurant naam en de status moet geselecteerd kunnen worden. | #53 | Ja | De bestellijsten worden gefiltered. Ook wordt het resultaat meteen weer in pagina's opgedeeld. | 
| Het testen van de registratie functionaliteit. Een gebruiker moet kunnen registreren, en indien dat gelukt/gefaalt is goede feedback terug krijgen | #1, #39, #40 | Ja | De gebruikers kunnen zich registreren op de registratie pagina en krijgt een popup als het gelukt is, of een error message als het gefaald is |
| De gebruiker moet een order lijst aan kunnen maken die vervolgens naar de backend wordt toegestuurd zodat die daar opgeslagen kan worden | #43, #45, #48, #51 | Ja | De orderlijsten worden aan gemaakt en naar de backend gestuurd. Ook kunnen meerdere mensen worden uitgenodigd en krijgt de gebruiker feedback gebaseerd of de orderlijst is aangemaakt of als er een error is |




