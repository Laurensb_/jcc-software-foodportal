# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 28.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| 1e dag sprint, geen taken                 |               |                 |             |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Bestellijst maken (pagina / issues)             | Roy           |             |
| Overzicht van bestellijsten (pagina / issues)   | Guus          |             |
| Back-end (REST files, mail service, middleware, etc.) | Thijs / Laurens |     |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
|     /                          |            |              |
|                                |            |              |
|                                |            |              |
|                                |            |              |
<br/> 





