# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 05.10.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Verder werken aan de front end / probleem met stylesheet oplossen. | Roy | Deels | Front-end pagina, nog niet helemaal klaar |
| Verder werken aan de fron end (bestellijsten aanmaken) | Guus | Deels | pagina, nog niet helemaal klaar |
| Verder werken aan de back-end | Thijs / Laurens | Deels | |
| Maken Sprint planning | Laurens | Ja | |
| Berekenen / Toevoegen focus factor sprint 1 | Thijs | Ja | |

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Verder met de front-end paginas                 | Guus / Roy    |             |
| Eventuele aanpassingen aan front-end            | Roy           | Nog een aantal puntjes op de i's zetten |
| Verder met de mailservice / mail format         | Laurens       |             |
| PUT Functies / verder met back-end              | Thijs         |             |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
| Nog niemand heeft hulp nodig, er wordt natuurlijk wel feedback gegeven / ondersteuning geboden | | |

<br/> 








