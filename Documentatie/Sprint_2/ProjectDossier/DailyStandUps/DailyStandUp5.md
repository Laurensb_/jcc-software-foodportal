# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 06.10.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| MailService (back-end)                    | Laurens       | Ja              | Opmaak zit er ook al volledig in |
| Filters in orderlist overzicht maken en orderlists klikbaar maken | Guus | Ja | |
| Multiselect voor de createOrderList pagina toevoegen | Roy | Ja | |
| PUT Functies (back-end) + helpen mailservice | Thijs | Ja | Laurens heeft de mailservice voornamelijk alleen gedaan |
| Commentaar in back-end toevoegen          | Thijs | Ja | Was geen issue voor deze sprint, maar is wel toegevoegd | 

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Opmaak mails verbeteren / Route voor het opvragen van mails | Laurens | |
| Bug fixes in gemaakte delen                     | Guus | |
| Verder met de createorderlist pagina / bug fixes etc. | Roy | Maken duurt langer dan verwacht |
| Opzet maken voor de presentatie voor morgen | Thijs | |
| Eigen delen in de presentatie zetten | Iedereen | Presentatie voor klant (Sprint review) |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
| Nog niemand heeft hulp nodig, er wordt natuurlijk wel feedback gegeven / ondersteuning geboden | | |

<br/> 









