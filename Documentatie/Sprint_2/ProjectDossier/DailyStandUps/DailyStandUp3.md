# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 29.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Verder werken aan "Bestellijst maken (pagina)" | Roy | In Progress | |
| Verder werken aan "Overzicht van bestellijsten (pagina)" | Guus | |
| Verder werken aan "Back-end (REST files, mail service, middleware, etc.)" | Thijs / Laurens | | 

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Verder werken aan de front end / probleem met stylesheet oplossen. | Roy | |
| Verder werken aan de front end (bestellijsten aanmaken) | Guus | |
| Verder werken aan de back-end | Thijs / Laurens | |
| Maken Sprint planning | Laurens | |
| Berekenen / Toevoegen focus factor sprint 1 | Thijs | |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
| Nog niemand heeft hulp nodig, er wordt natuurlijk wel feedback gegeven / ondersteuning geboden | | |

<br/> 







