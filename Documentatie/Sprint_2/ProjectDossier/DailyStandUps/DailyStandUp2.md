# **Rapportage Daily Standups**

| Teamnummer: 2, Klas: EHI2V.Sa | 29.09.2021 | 
| ----------------------------- | ---------- |
<br/>

| Present            | Absent (+Waarom?) |
| ------------------ | ----------------- |
| Roy Weghorst       |                   |
| Laurens Bittner    |                   |
| Guus van der Vegt  |                   |
| Thijs van der Vegt |                   |
<br/>

| Taken van vorige keer (Geef beschrijving) | Toegekend aan | Afgerond Ja/Nee | Opmerkingen | 
| ----------------------------------------- | ------------- | --------------- | ----------- |
| Begin maken met "Bestellijst maken (pagina / issues)" | Roy | Begonnen | 1e dag na vorige standup, issues wel klaar. |
| Begin maken met "Overzicht van bestellijsten (pagina / issues)" | Guus | Begonnen | |
| Begin maken met "Back-end (REST files, mail service, middleware, etc.)" | Thijs / Laurens | Begonnen | middleware geupdate | 

<br/>

| Taken voor de volgende keer (Geef beschrijving) | Toegekend aan | Opmerkingen | 
| ----------------------------------------------- | ------------- | ----------- |
| Verder werken aan "Bestellijst maken (pagina)"  | Roy           |             |
| Verder werken aan "Overzicht van bestellijsten (pagina)" | Guus  |             |
| Verder werken aan "Back-end (REST files, mail service, middleware, etc.)" | Thijs / Laurens | |

<br/>

| Wie heeft ondersteuning nodig? | Wie helpt? | Beschrijving | 
| ------------------------------ | ---------- | ------------ | 
| Nog niemand heeft hulp nodig, er wordt natuurlijk wel feedback gegeven / ondersteuning geboden | | |

<br/> 






