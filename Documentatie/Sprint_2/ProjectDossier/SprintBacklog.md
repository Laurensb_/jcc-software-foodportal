# Sprint Backlog
In de sprint backlog worden alle User Stories getoont die waar wij in sprint 2 aan gaan werken. 
In de issues zelf zijn de 'How to test' en 'How to demo' te zien. (In de functionele issues).

## Definition of Done
Een User Story is 'done' wanneer het aan de volgende punten voldoet:
* De User Story is minimaal twee keer getest, door iemand anders dan degene die de User Story gemaakt heeft.
* De User Story is minimaal 1 keer goed door de test heen gekomen. 
* De User Story is ook gecontroleerd / er is feedback gegeven door iemand anders uit het team.
* De User Story is geclosed op GitLab. 

## Issues
![1e Gedeelte lijst issues](../Images/Issues1.png "Issues")<br>
![2e Gedeelte lijst issues](../Images/Issues2.png "Issues")<br>
![3e Gedeelte lijst issues](../Images/Issues3.png "Issues")<br>
![4e Gedeelte lijst issues](../Images/Issues4.png "Issues")<br>
