# Sprint Planning

De volgende user stories gaan in de 2e sprint worden uitgewerkt:<br>
![1e Gedeelte lijst issues](../Images/Issues1.png "Issues")<br>
![2e Gedeelte lijst issues](../Images/Issues2.png "Issues")<br>
![3e Gedeelte lijst issues](../Images/Issues3.png "Issues")<br>
![4e Gedeelte lijst issues](../Images/Issues4.png "Issues")<br>

## Deadline
Voor elke sprint worden ongeveer 2 weken gerekend. Voor sprint 2 zal de deadline dan op **10-10-2021** liggen. Zoals beschreven in de werkafspraken is er ruimte voor 24 uur uitloop, mits dit overlegt is binnen het team. Deze uitloop is alleen bedoelt voor werk die door onvoorziene omstandigheden niet uitgevoerd had kunnen worden.

## Taakverdeling
Hieronder is globaal beschreven wat de taken per teamlid zijn. Ook worden de userstories die dit gaat afwerken vermeld.

### Laurens 
- aan het backend werken met de emailservice
- aan het backend zorgen dat de rest calls gedocumenteerd zijn
- entities waar nodig aanpassen

**Userstories:**
\#6, \#10, \#31, \#32, \#34, \#55

### Thijs
- aan het backend werken met de emailservice
- aan het backend zorgen dat de rest calls gedocumenteerd zijn
- entities waar nodig aanpassen

**Userstories:**
\#20, \#42, \#54, \#55

### Roy
- werken aan de pagina om orderlijsten aan te maken

**Userstories:**
\#3, \#43, \#45, \#48, \#51

### Guus
- werken aan de overzicht pagina
- verbeteringen front end

**Userstories:**
\#7, \#42, \#47, \#49, \#52, \#53

## Doel Sprint 2
Het doel van de 2e sprint is om het portaal volledig opgebouwd te hebben, een gebruiker zou nu een lijst aan moeten kunnen maken en ook een bestelling kunnen plaatsen. ook zullen er een aantal verbeteringen komen bij het reistreren en inloggen. voor al deze functionaliteit moet ook het backend bijgewerkt worden en met het backend moeten we ook emails gaan versturen.

## Eindproduct
Het eindproduct zal deze sprint een frontend applicatie zijn, waar de gebruiker volledig doorheen kan, een account kan aanmaken, een orderijst kan aanmaken en een bestelling kan plaatsen. hier komen wel een aantal restricties bij, voor mensen die speciaal iets mogen doen. Ook zullen we emails kunnen sturen van het backend voor confirmatie en bestellijsten versturen.

