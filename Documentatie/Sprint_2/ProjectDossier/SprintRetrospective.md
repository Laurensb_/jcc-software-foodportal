# Sprint Retrospective

## Verbeteringen van vorige keer:
* De communicatie gaat onderling al een stuk beter, en daar zijn we erg tevreden mee. We communiceren nu altijd wanneer we problemen hebben, ook verlenen we daarbij dan hulp als dat nodig is. We worden ook goed op de hoogte gehouden door groepsleden, wanneer een deel af is.
* Deze sprint hebben we al ons werk over de hele 2 weken op het begin ingeplant. Dit is wel goed gedaan, maar we hebben er ons niet helemaal aan gehouden.
* Deze sprint hebben elke keer, wanneer het werk van iemand klaar was, er naar gekeken en ook het getest.

## Goede punten:
* Goede taakverdeling, iedereen wist hij moest doen.
* Nakomen van afspraken, iedereen had zijn werk op tijd af, en het was op een serieuze manier gemaakt.
* Kwaliteit, naar onze mening is het product wat opgeleverd is van goede kwaliteit. 
* Feedback, als iemand met het werkt van een persoon verder ging, dan werd er kritisch gekeken naar het gemaakte werk.

## Minder goede punten:
* Tijdsplanning, het was duidelijk wanneer de deadline was, ook hadden we wel een goede planning gemaakt, maar werken volgens de planning ging niet het beste
* Communicatie, er werd goed gecommuniceerd, alleen het kan nog vaker.
* Tijdgebruik, door sommiggen was er redelijk weinig tijd in het project gestopt.

## Verbeteringen
* Om het tijdgebruik te verbeteren, gaan we vaker samen aan het werk. En dat controleren we ook van anderen. Naast dit gaan we ook kijken hoeveel uren iemand per dag in het project stopt, en als het per dag niet ongeveer op ~1 uur komt, dan zullen we deze persoon gaan aanspreken, als het werk niet goed af is.
* Momenteel wachten we vaak op iemand anders voor een opmerking, we willen nu dat je naar iemand in het groepje toe gaat, om te vragen of diegene ergens naar wil kijken en dan wat opmerkingen geeft.
* Om beter de tijdsplanning te volgen, zullen we anderen in onze groep goed gaan controlleren, of er progressie wordt gemaakt tot het einddoel. Als dit niet zo is, zullen we  de persoon in aanmerken hier op aanspreken.
