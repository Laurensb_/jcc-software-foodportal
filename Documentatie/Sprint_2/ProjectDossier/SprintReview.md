# Sprint 2 Review

### Doel Sprint: Het automatiseren van het versturen van emails, het maken van pagina's gerelateerd aan bestel lijsten
---

### Onderwerpen:
1. De applicatie moet emails kunnen versturen
2. De applicatie moet een werkende pagina voor het aanmaken van bestel lijsten hebben
3. De applicatie moet een werkende pagina voor het overzicht van bestel lijsten hebben

---

### User Stories:  <br/>
#### Afgeronde User Stories:
![Eerste afbeelding user stories](../Images/sprint2UserStories1.PNG)
![Tweede afbeelding user stories](../Images/sprint2UserStories2.PNG)
![Derde  afbeelding user stories](../Images/sprint2UserStories3.PNG)

#### Onafgeronde User Stories:
Er zijn geen userstories niet afgerond deze sprint

---

### Review:
Tijdens deze sprint was het doel om de emailer te maken en om de pagina's voor zowel het overzicht als het aanmaken van de bestel lijsten aan te maken. 

Voor de mailer is gebruikt gemaakt van de 'nodemailer' library, eerst werdt ook 'ethereal' gebruikt om mockup mails te versturen. Ethereal is alleen nu niet langer functioneel, en daarom verweiderd uit het project.

de rest van deze sprint verliep soepel met niet veel om hier te melden

---

### Burn-down chart:

![Burn-down chart afbeelding](../Images/burnDownChart.PNG)

De Burn-down chart van deze sprint is wel correct gemaakt. Ook laat het zien dat het werk af was voor dat de sprint was geeindigd. Dit komt omdat de sprint review presentatie een paar dagen voor het officiele eind van de sprint werdt gehouden, en de groep op die datum alles af wou hebben in plaats van het officiele einde van de sprint

---

### Focus Factor
De focus factor wordt berekend door de velocity te delen door het aantal gemaakte uren.
Het aantal beschikbare uren zijn 50 uren per week. Sprint 2 bestond uit 2 weken, dus hadden we 100 beschikbare uren. Hiervan hebben wij er 93.38 gemaakt. 

De velocity is hoeveel Storypoints we in deze sprint hebben behandeld, dit zijn er in totaal 60. 

Dus -> "Velocity" / "Gebruikte uren" --> 60/93.38 = 0.64

Dus onze focus factor voor de sprint was 0.64, dit getal is hoger dan vorige sprint, dit wil dus zeggen dat we efficiënter zijn gaan werken.

We kunnen met dit getal wel het aantal story points die wij voor de volgende sprint aan zouden moeten kunnen berekenen. 
Dat is namelijk -> 0.64 x 100 = 64. dit betekent dat wij in sprint 3 ongeveer 64 storypoints aankunnen. 




