export const getAmountOfPages = (amountOfItems, perPage) => {
    return Math.ceil(amountOfItems / perPage)
}

export const paginate = (data, pageNumber, perPage) => {
    let totalItems = data.length;
    const amountOfPages = getAmountOfPages(totalItems, perPage)
    
    if(pageNumber < 1) pageNumber = 1
    else if(pageNumber > amountOfPages) pageNumber = amountOfPages 

    let paginated = [];


    for (let i = (pageNumber - 1) * perPage; i < (pageNumber * perPage) && i < totalItems; i++) {
        paginated.push(data[i])
    }

    return paginated;
}