export const getItemFromLocalStorage = (key) => {
    let data = localStorage.getItem(key);
    
    if(data){
        data = JSON.parse(data);
        if(new Date().getTime() > data.expiry){
            removeItemFromLocalStorage(key)
            return null
        }

        return data.value;
    }

    return null;
}

export const setItemInLocalStorage = (key, item) => {
    if(key && item){

        const data = {
            value: item,
            expiry: new Date().getTime() + (86400 * 1000)
        }
        localStorage.setItem(key, JSON.stringify(data))
    }
}

export const removeItemFromLocalStorage = (key) => {
    if(key) {
        localStorage.removeItem(key);
    }
}