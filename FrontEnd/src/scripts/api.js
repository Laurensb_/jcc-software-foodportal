import user from "../store/user";

const get = async (endpoint, params = {}) => {
    try {
        let config = getConfig("GET", {});

        return await fetch(getEndpoint(endpoint, params), config).then(response => response.json())
    } catch (err){
        return err;
    }
}

const post = async (endpoint, data, params = {}, ) => {
    let config = getConfig("POST", data)

    try {
        return await fetch(getEndpoint(endpoint), config).then(response => response.json())
    } catch (err) {
        console.log("Error here")
        throw new Error(err)
        return err;
    }
}

const put = async (endpoint, data, params = {}) => {
    let config = getConfig("PUT", data);
    try {
        return await fetch(getEndpoint(endpoint), config).then(response => response.json())
    } catch (err) {
        throw new Error(err)
    }
}

const deleteRequest = async (endpoint) => {
    try {
        let config = getConfig("DELETE", {});

        const response = await fetch(getEndpoint(endpoint), config)
        const data = await response.json();

        return data;

    } catch (err){
        return err;
    }
}

const getEndpoint = (endpoint, params) => {
    if(endpoint[0] == "/") endpoint = endpoint.substring(1);

    endpoint = "http://localhost:3000/" + endpoint;

    if(params && typeof params == "object") {
        let paramString = "?"

        Object.keys(params).forEach(key => {
            paramString = `${paramString}${key}=${params[key]}&`

        })

        endpoint = endpoint + paramString;
    }

    return endpoint

    // if(endpoint[0] == "/") endpoint = endpoint.substring(1);
    // return "http://localhost:3000/" + endpoint
}

const getConfig = (method, data = null, params = null) => {

    method = method.toUpperCase();
    if(method !== "POST" && method !== "GET" && method !== "PUT" && method !== "DELETE") throw new Error("Specified HTTP method not allowed");

    console.log(JSON.stringify(data))
    let config = {
        method,
        // mode: "cors",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        }
    }

    if(method === "GET") delete config.body;

    let token = null;

    const unsubscribe = user.getToken().subscribe(data => {
        token = data;
    })

    if(token) {
        config.headers["Authorization"] = `Bearer ${token}`
    }

    unsubscribe();
    return config
}

export default {
    get,
    post,
    put,
    deleteRequest
}