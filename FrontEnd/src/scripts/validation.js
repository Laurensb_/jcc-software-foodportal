export const validateEmail = (email) => {
    const regex = /(\w+.)+@(\w*.)+\.(com|nl)/g;
    return regex.test(String(email).toLowerCase())
}