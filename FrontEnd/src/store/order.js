import {writable} from "svelte/store";
import api from "../scripts/api";

const orders = writable([]);
const order = writable(null)
const error = writable(null)

const getOrders = (orderlistId = null) =>{
    api.get(`/orderlists/${orderlistId}/orders`)
    .then(responseData => {
        setOrders(responseData)
    })
}

const getAllOrders = () =>{
    api.get(`/orders`)
        .then(responseData => {
            setOrders(responseData)
        })
}

const createOrder = (data) => {
    if(!data.description || !data.paymentMethod) {
        const errorMessage = "All fields are required!";
        setError(errorMessage);
        return;
    }

    api.post("/orders", data)
        .then(responseData => {
            if(responseData.description !== undefined) {
                let tempOrders = [];

                orders.subscribe(data => {
                    tempOrders = data;
                })();

                tempOrders.push(responseData)
                setOrders(tempOrders)
            }
        })
}

const setError = (message) => {
    error.set(message);
}

const setOrders = (ordersData) => {
    orders.set(ordersData)
}

const updateOrder = (newOrder) => {
    let tempOrders = [];

    orders.subscribe(data => {
        tempOrders = data;
    })();

    let index = tempOrders.findIndex(item => item.id === newOrder.id)
    tempOrders[index] = newOrder;
    setOrders(tempOrders);
}

const setHasPayed = (orderId, status) => {
    let tempOrder = {}
    orders.subscribe((data) => {
        tempOrder = JSON.parse(JSON.stringify(data.filter(item => item.id === orderId)[0]))
    })()

    tempOrder.isPayed = status
    delete tempOrder.id
    delete tempOrder.userID

    api.put(`/orders/${orderId}`, tempOrder)
    .then((responseData) => {
        updateOrder(responseData)
    })
}

const deleteOrder = (orderId) => {

    api.deleteRequest(`/orders/${orderId}`)
    .then(() => {
        let tempOrders = [];

        orders.subscribe(data => {
            tempOrders = data.filter(item => item.id != orderId);
        })();

        setOrders(tempOrders)
    })
}


export default {
    order,
    orders,
    error,
    getOrders,
    getAllOrders,
    createOrder,
    setError,
    setHasPayed,
    deleteOrder
}