import { writable } from "svelte/store";
import api from "../scripts/api";
import { validateEmail } from "../scripts/validation";
import { navigate } from "svelte-routing";
import { getItemFromLocalStorage, removeItemFromLocalStorage, setItemInLocalStorage } from "../scripts/storage";
import { decodeToken } from "../scripts/jwt"
import {validatePassword} from "../../../BackEnd/Utils/validations";

const user = writable(null)
const token = writable(null)
const error = writable(null)


const login = (data) => {
    if(!data.email || !data.password){
        const errorMessage = "Email and password are required!"
        setError(errorMessage)
        return
    }

    if(!validateEmail(data.email)){
        const errorMessage = "Email is not valid"
        setError("Email is not valid")
        return

    }

    api.post("/auth", data)
    .then(responseData => {
        setToken(responseData.token);

        const payloadUser = decodeToken(responseData.token);
        setUser(payloadUser)

        navigate("/", {replace: true})
    })
    .catch((err) => {
        console.log(err)
        setError("Username or Password incorrect!")
    })
}

const register = (data) => {
    if(!data.email || !data.firstName || !data.lastName || !data.password) {
        const errorMessage = "All fields are required!";
        setError(errorMessage);
        return;
    }

    if(!validateEmail(data.email)){
        const errorMessage = "Email is not valid"
        setError("Email is not valid")
        return;

    }

    if(!validatePassword(data.password)) {
        const errorMessage = "Password is not valid"
        setError("Password is not valid")
        return;
    }

    api.post("/users", data)
        .then(res => {
            window.alert('User created.')
            error.set(null);
            navigate("/login");
        })
}

const requestRandomPass = (email) => {
    let data = {
        email: email,
        type: "mailpass"
    }
    api.post("/mails", data).then(r => {
        return true;
    })
}

const updateUser = (data, id) => {
    return api.put("/users/" + id, data).then(r => {
        if (r.error) {
            window.alert(r.error)
        } else {
            logout()
            window.location = "/login"
        }
    })
}

const logout = () => {
    removeItemFromLocalStorage("jwtToken")
    setUser(null);
}

const setUser = (data) => {
    user.set(data);
}

const getUser = () => {
    return user
}

const setToken = (data) => {
    setItemInLocalStorage("jwtToken", data)
    token.set(data);
}

const getToken = () => {
    return token;
}

const setError = (message) => {
    error.set(message)
}

const getError = () => {
    return error;
}


const getUUID = () => {
    let uuid;

    const unsubscribe = user.subscribe(data => uuid = data.id)
    unsubscribe();

    return uuid;
}

const getRoles = () => {
    let roles = []

    const unsubscribe = user.subscribe(data => roles = data.roles)
    unsubscribe();

    return roles;
}

const restoreFromSessionStorage = () => {
    const jwtToken = getItemFromLocalStorage("jwtToken");
    setToken(jwtToken)

    if(jwtToken) {
       const payloadUser = decodeToken(jwtToken);
       setUser(payloadUser); 
    }
}



export default {
    error,
    user,
    token,
    setUser,
    setToken, 
    getUser, 
    getToken, 
    getError, 
    login,
    register,
    logout,
    getUUID,
    getRoles,
    restoreFromSessionStorage,
    requestRandomPass,
    updateUser
}

