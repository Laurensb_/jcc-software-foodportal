import { derived, writable } from "svelte/store";
import api from "../scripts/api.js";
import { paginate } from "../scripts/pagination.js"
import user from "./user.js";
import {navigate} from "svelte-routing";


const searchTerm = writable("");
const status = writable("active");
const type = writable("all");
const loading = writable(false);
const rawOrderList = writable([])
const orderList = writable({});
const error = writable(null);


const filteredOrderList = derived(
    [searchTerm, status, type, rawOrderList],
    ([$searchTerm, $status, $type, $items]) => {
        let data = $items;

        if($type === "created"){
            data = data.filter(x => x.organiser === user.getUUID())
        }
        
        if($type === "invited"){
            data = data.filter(x => x.organiser !== user.getUUID())
        }


        if($status === "active" ){
            data = data.filter(x => !x.hasEnded) 
        }

        if($status === "expired") {
            data = data.filter(x => x.hasEnded) 
        }

        if($searchTerm.length > 0){
            data = data.filter(x => x.restaurantName.toLowerCase().includes($searchTerm.toLowerCase())) 
        }

        
        return data;
    }
)

const paginatedOrderList = (page, perPage) => {
    let orderLists = [];
    const unsubscribe = filteredOrderList.subscribe(value => {
        orderLists = value
    })

    unsubscribe();

    if(orderLists.length <= 0) return []
    return paginate(orderLists, page, perPage);
}

const fetch = () => {
    loading.set(true);

    return api.get('/orderlists')
    .then(response => {
        setRawOrderList(response)
    })
    .finally(() => {
        loading.set(false);
    })
}

const createOrderlist = (data) => {
    if (!data.title || !data.restaurantName || !data.linkToMenu || !data.endDate) {
        const errorMessage = "All fields are required!";
        setError(errorMessage);
        throw new Error(errorMessage)
    }

    api.post("/orderlists", data)
        .then(responseData => {
            if (responseData.title !== undefined) {
                navigate("/");
            }
        })
}

const updateOrderList = (data, id) => {
    if (!data.title || !data.restaurantName || !data.linkToMenu || !data.endDate) {
        const errorMessage = "All fields are required!";
        setError(errorMessage);
        throw new Error(errorMessage)
    }

    api.put(`/orderlists/${id}`, data)
        .then(response => {
        if(response.title !== undefined){
            navigate(`/orderlists/${id}`, {replace: true})
        }
    })


}

const getOrderListByID = (id) => {
    return api.get("/orderlists").then(response => {
        return response.find(list => {
            return list.id === id
        })
    })
}

const setOrderListById = (id) => {
    let orderListPresent = Object.keys(getRawOrderList()).length > 0;

    if(orderListPresent){
        const list = getRawOrderList().filter(item => item.id === id)[0]

        orderList.set(list);
    }
    else {
        loading.set(true);
        api.get(`orderlists/${id}`)
        .then(data => {
            orderList.set(data);

        })
        .finally(() => {
            loading.set(false);
        })
    }
}

const getSpecificUserOrders = (orderlistID, userID) => {

    return api.get(`orderlists/${orderlistID}/orders`, {userID})
        .then(response => {
            return response;
        })
}

const setSearchTerm = (term) => {
    searchTerm.set("term");
}

const getSearchTerm = () => {
    return searchTerm;
}

const getFilteredOrderList = () => {
    return filteredOrderList
}

const setRawOrderList = (data) => {
    rawOrderList.set(data);
}

const getRawOrderList = () => {
    let data = [];

    const unsubscribe = rawOrderList.subscribe(lists => {
        data = lists
    });

    unsubscribe();

    return data;
}

const setError = (message) => {
    error.set(message);
}

export default {
    getOrderListByID,
    loading,
    searchTerm,
    status,
    type,
    rawOrderList,
    error,
    paginatedOrderList,
    filteredOrderList,
    getFilteredOrderList,
    fetch,
    setRawOrderList, 
    getRawOrderList, 
    setSearchTerm,
    getSearchTerm,
    setOrderListById,
    orderList,
    getSpecificUserOrders,
    createOrderlist,
    updateOrderList
}